package com.itis.domain

import cats.implicits.catsSyntaxOptionId
import com.itis.service.Converter
import io.circe.{Decoder, Encoder, HCursor, Json}
import sttp.tapir.Schema

object errors {
  sealed abstract class AppError(val message: String, val cause: Option[Throwable] = None) {
    def exString: String =
      cause.map(ex => s"Message: ${ex.getMessage}\n${ex.getStackTrace.mkString("\n")}").getOrElse("")
  }
  object AppError {
    implicit val encoder: Encoder[AppError] = (appError: AppError) =>
      Json.obj(("message", Json.fromString(appError.message)))
    implicit val decoder: Decoder[AppError] = (c: HCursor) => c.downField("message").as[String].map(MockException)
    implicit val scheme: Schema[AppError] = Schema.string[AppError]

  }

  case class InternalError(cause0: Throwable) extends AppError(cause0.getMessage, cause0.some)

  object InternalError {
    implicit val encoder: Encoder[InternalError] = (internalError: InternalError) =>
      Json.obj(("message", Json.fromString(internalError.message)))
    implicit val decoder: Decoder[InternalError] = (c: HCursor) =>
      c.downField("message").as[String].map(_ => InternalError(new Exception()))
    implicit val scheme: Schema[InternalError] = Schema.string[InternalError]
  }

  sealed abstract class JwtError(override val message: String) extends AppError(message)

  object JwtError {
    implicit val toInternalErrorConv: Converter[JwtError, InternalError] = jwtError =>
      InternalError(new Exception(jwtError.message))
    implicit val toAppError: Converter[JwtError, AppError] = identity(_)
  }

  case class JwtDecodingError(override val message: String) extends JwtError(message)

  case class UserNotFound(anyId: String) extends AppError(s"User with id $anyId not found")

  case class AuthError(override val message: String) extends AppError(message)

  case class UserAlreadyExists(createUser: CreateUser)
    extends AppError(
      s"Could not create user $createUser" +
        ", such user already exists"
    )

  case class MockException(override val message: String) extends AppError(message)
}
