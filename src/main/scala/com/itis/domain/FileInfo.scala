package com.itis.domain

import com.itis.domain.types.FileId

import java.time.LocalDateTime

case class FileInfo(id: FileId, originalName: String, storageName: String, size: Long, creationTime: LocalDateTime)
