package com.itis.domain

import derevo.circe.{decoder, encoder}
import derevo.derive
import io.estatico.newtype.macros.newtype
import tofu.logging.derivation.loggable

import java.time.LocalDate
import doobie._
import doobie.postgres.implicits._
import org.checkerframework.checker.units.qual.s
import sttp.tapir.Schema
import sttp.tapir.derevo.schema
//import sttp.tapir.

object types {
  @derive(loggable, encoder, decoder, schema)
  @newtype case class PasswordHash(value: String)
  object PasswordHash {
    implicit val doobieRead: Read[PasswordHash] = Read[String].map(PasswordHash(_))
  }

  @derive(loggable, encoder, decoder, schema)
  @newtype case class JwtToken(value: String)

  @derive(loggable, encoder, decoder, schema)
  @newtype case class Email(value: String)

  object Email {
    implicit val doobieRead: Read[Email] = Read[String].map(Email(_))
  }

  @derive(loggable, encoder, decoder, schema)
  @newtype case class PlainPassword(value: String)
  @derive(loggable, encoder, decoder, schema)
  @newtype case class Salt(value: String)
  @derive(loggable, encoder, decoder, schema)
  @newtype case class Phone(value: String)
  object Phone {
    implicit val doobieRead: Read[Phone] = Read[String].map(Phone(_))
  }

  @derive(loggable, encoder, decoder, schema)
  @newtype case class UserId(value: String)
  object UserId {
    implicit val doobieRead: Read[UserId] = Read[String].map(UserId(_))
  }

  @derive(loggable, encoder, decoder, schema)
  @newtype case class FileId(value: String)

  object FileId {
    implicit val doobieRead: Read[FileId] = Read[String].map(FileId(_))
    implicit val doobiePut: Put[FileId] = Put[String].contramap(productId => productId.value)
    implicit val doobieGet: Get[FileId] = Get[String].map(FileId(_))
  }

  @derive(loggable, encoder, decoder, schema)
  @newtype case class CategoryId(value: Long)

  object CategoryId {
    implicit val doobieRead: Read[CategoryId] = Read[Long].map(CategoryId(_))
    implicit val doobieGet: Get[CategoryId] = Get[Long].map(CategoryId(_))
  }
  @derive(loggable, encoder, decoder, schema)
  @newtype case class ProductId(value: Long)

  object ProductId {
    implicit val doobieRead: Read[ProductId] = Read[Long].map(ProductId(_))
    implicit val doobiePut: Put[ProductId] = Put[Long].contramap(productId => productId.value)
    implicit val doobieGet: Get[ProductId] = Get[Long].map(ProductId(_))
  }
  @derive(loggable, encoder, decoder, schema)
  @newtype case class OrderId(value: Long)

  object OrderId {
    implicit val doobieRead: Read[OrderId] = Read[Long].map(OrderId(_))
  }

  @derive(loggable, encoder, decoder, schema)
  @newtype case class BasketId(value: Long)

  object BasketId {
    implicit val doobieRead: Read[BasketId] = Read[Long].map(BasketId(_))
  }
}
