package com.itis.domain

import com.itis.domain.types.{BasketId, UserId}

import java.time.LocalDateTime

case class Basket(id: BasketId, customerId: UserId, totalPrice: Int, updateTime: LocalDateTime)

case class BasketUpdate(id: BasketId, customerId: UserId)

case class NewBasket(customerId: UserId)
