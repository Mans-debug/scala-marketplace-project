package com.itis.domain

import com.itis.domain.types._
import doobie.{Get, Put}
import doobie.util.Write
import shapeless.ops.nat.Sum

import java.time.LocalDateTime

case class Product(
  id: ProductId,
  title: String,
  sellerId: UserId,
  price: Int,
  categoryId: CategoryId,
  quantity: Int,
  description: Option[String],
  creationTime: LocalDateTime
)

case class ProductFile(productId: ProductId, fileId: FileId)

case class ProductFilter(name: Option[String],
                         category: Option[CategoryId],
                         priceLow: Option[Int],
                         priceHigh: Option[Int])

case class NewProduct(
  title: String,
  sellerId: UserId,
  price: Int,
  categoryId: CategoryId,
  quantity: Int,
  description: Option[String]
)

object NewProduct {
  def toProduct(id: ProductId, newProduct: NewProduct): Product =
    Product(
      id,
      newProduct.title,
      newProduct.sellerId,
      newProduct.price,
      newProduct.categoryId,
      newProduct.quantity,
      newProduct.description,
      LocalDateTime.now()
    )
}

case class ProductBasket(id: Long, productId: ProductId, basketId: BasketId, quantity: Int)

case class NewProductBasket(productId: ProductId, basketId: BasketId, quantity: Int)

object NewProductBasket {
  def toProductBasket(id: Long, newProductBasket: NewProductBasket): ProductBasket =
    ProductBasket(
      id,
      newProductBasket.productId,
      newProductBasket.basketId,
      newProductBasket.quantity
    )
}
