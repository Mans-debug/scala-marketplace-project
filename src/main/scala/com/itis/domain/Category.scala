package com.itis.domain

import com.itis.domain.types.CategoryId

case class Category(id: CategoryId, parentId: Option[CategoryId], title: String)

case class NewCategory(parentId: Option[CategoryId], title: String)

object NewCategory {
  def toCategory(id: CategoryId, category: NewCategory): Category = Category(id, category.parentId, category.title)
}
