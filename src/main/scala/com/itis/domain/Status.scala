package com.itis.domain

case class Status(id: Long, name: String)

case class NewStatus(name: String)
