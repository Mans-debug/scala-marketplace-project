package com.itis.domain

import com.itis.controller.endpoints.user
import com.itis.domain.types._
import io.circe.{Encoder, Json}
import org.http4s.RangeUnit.None
import org.mindrot.jbcrypt.BCrypt

import java.time.LocalDate
import java.time.format.DateTimeFormatter

case class SignUpForm(
  name: String,
  role: Role,
  email: Email,
  surname: Option[String],
  patronymic: Option[String],
  password: PlainPassword,
  gender: Option[String],
  birthDate: Option[String],
  phone: Phone
)

case class SignInForm(email: Option[Email], phone: Option[Phone], password: PlainPassword)

case class CreateUser(
  name: String,
  role: Role,
  email: Email,
  surname: Option[String],
  patronymic: Option[String],
  passwordHash: PasswordHash,
  gender: Option[String],
  birthDate: Option[String],
  phone: Phone
)

object CreateUser {
  def fromSignUpForm(signUpForm: SignUpForm, salt: Salt): CreateUser = {
    def hashPlainPassword(password: PlainPassword) = {
      PasswordHash(BCrypt.hashpw(password.value, salt.value))
    }

    CreateUser(
      signUpForm.name,
      signUpForm.role,
      signUpForm.email,
      signUpForm.surname,
      signUpForm.patronymic,
      hashPlainPassword(signUpForm.password),
      signUpForm.gender,
      signUpForm.birthDate,
      signUpForm.phone
    )
  }

  def toUser(userId: UserId, createUser: CreateUser): User = {
    User(
      userId,
      createUser.role,
      createUser.email,
      createUser.name,
      createUser.surname,
      createUser.patronymic,
      createUser.passwordHash,
      createUser.gender,
      createUser.birthDate,
      createUser.phone,
      scala.None,
      0,
      scala.None
    )
  }
}

case class UserPrincipal(id: UserId, roles: Role)
case class UserPrincipalWithToken(id: UserId, roles: Role, jwt: JwtToken)

case class User(
  id: UserId,
  role: Role,
  email: Email,
  name: String,
  surname: Option[String],
  patronymic: Option[String],
  passwordHash: PasswordHash,
  gender: Option[String],
  birthDate: Option[String],
  phone: Phone,
  profilePicture: Option[FileId],
  points: Int,
  city: Option[String]
)

object User {
  implicit val encoder: Encoder[User] = user =>
    Json.obj(
      ("id", Json.fromString(user.id.value)),
      ("name", Json.fromString(user.name)),
      ("surname", user.surname.map(Json.fromString).getOrElse(Json.Null)),
      ("patronymic", user.patronymic.map(Json.fromString).getOrElse(Json.Null)),
      ("gender", user.gender.map(Json.fromString).getOrElse(Json.Null)),
      ("birthDate", user.birthDate.map(_.format(DateTimeFormatter.ISO_DATE)).map(Json.fromString).getOrElse(Json.Null)),
      ("phone", Json.fromString(user.phone.value)),
      ("profilePicture", user.profilePicture.map(_.value).map(Json.fromString).getOrElse(Json.Null)),
      ("points", Json.fromInt(user.points)),
      ("city", user.city.map(Json.fromString).getOrElse(Json.Null))
   )
}
