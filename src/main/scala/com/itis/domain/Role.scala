package com.itis.domain

import doobie.Read
import io.circe.Decoder.Result
import io.circe.{Codec, HCursor, Json}
import sttp.tapir.Schema

sealed trait Role {
  def name: String
}

object Role {
  implicit val schema: Schema[Role] = Schema.string[Role]
  private val roleMap: Map[String, Role] = Map(
    Customer.name -> Customer,
    Seller.name -> Seller
  )
  implicit val read: Read[Role] = Read[String].map(roleMap.getOrElse(_, Customer))

  implicit val codec: Codec[Role] = new Codec[Role] {
    override def apply(a: Role): Json = Json.fromString(a.name)

    override def apply(c: HCursor): Result[Role] = c.as[String].map(roleMap.getOrElse(_, Customer))
  }
}

case object Customer extends Role {
  override def name: String = "CUSTOMER"
}

case object Seller extends Role {
  override def name: String = "SELLER"
}
