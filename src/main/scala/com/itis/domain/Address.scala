package com.itis.domain

case class Address(id: Long, fullAddress: String)

case class NewAddress(fullAddress: String)
