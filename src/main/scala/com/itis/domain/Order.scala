package com.itis.domain

import com.itis.domain.types.{OrderId, ProductId, UserId}
import io.circe.Decoder

import java.time.LocalDateTime

case class Order(id: OrderId, addressId: Long, customerId: UserId, totalPrice: Int, creationTime: LocalDateTime)

case class NewOrder(addressId: Long, customerId: UserId, totalPrice: Int)

object NewOrder {
  def toOrder(orderId: OrderId, newOrder: NewOrder): Order =
    Order(
      orderId,
      newOrder.addressId,
      newOrder.customerId,
      newOrder.totalPrice,
      LocalDateTime.now()
    )
}

case class OrderItem(
  id: Long,
  productId: ProductId,
  orderId: OrderId,
  statusId: Long,
  quantity: Int,
  deliveryTime: Option[LocalDateTime]
)

case class NewOrderItem(
  productId: ProductId,
  orderId: OrderId,
  statusId: Long,
  quantity: Int,
  deliveryTime: Option[LocalDateTime]
)

object NewOrderItem {
  def toOrderItem(id: Long, newOrderItem: NewOrderItem): OrderItem =
    OrderItem(
      id,
      newOrderItem.productId,
      newOrderItem.orderId,
      newOrderItem.statusId,
      newOrderItem.quantity,
      newOrderItem.deliveryTime
    )
}
