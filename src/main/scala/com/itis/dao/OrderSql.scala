package com.itis.dao

import com.itis.controller.endpoints.user
import com.itis.domain.types._
import com.itis.domain.{NewOrder, Order}
import doobie._
import doobie.implicits._
import doobie.postgres.implicits._

import java.sql.SQLException
import java.time.LocalDateTime

trait OrderSql {
  def findBySellerId(userId: UserId): ConnectionIO[Either[SQLException, List[Order]]]

  def create(order: NewOrder): ConnectionIO[Either[SQLException, Order]]

  def findById(orderId: OrderId): ConnectionIO[Option[Order]]

  def update(order: Order): ConnectionIO[Either[SQLException, Order]]

  def delete(orderId: OrderId): ConnectionIO[Either[SQLException, Int]]

  def findByUserId(userId: UserId): ConnectionIO[Either[SQLException, List[Order]]]
}

object OrderSql {
  private object sqls {
    def findBySellerId(userId: UserId): doobie.Query0[Order] =
      sql"""select orders.id, address_id, customer_id,
      |       (select coalesce(sum(p.price * order_items.quantity), 0) as total_price
      |        from order_items
      |                 inner join products p on p.id = order_items.product_id
      |                 inner join orders o on o.id = order_items.order_id
      |        where o.id = orders.id),
      |       orders.creation_time
      |from orders
      |         inner join order_items oi on orders.id = oi.order_id
      |         inner join products p on p.id = oi.product_id
      |where p.seller_id = ${userId.value}::uuid
      |""".stripMargin.query[Order]

    def findByUserId(userId: UserId): doobie.Query0[Order] =
      sql"""with product_sum as (select coalesce(sum(p.price * order_items.quantity), 0) as total_price
        |                     from order_items
        |                     inner join products p on p.id = order_items.product_id
        |                     inner join orders o on o.id = order_items.order_id
        |                     where o.customer_id = ${userId.value}::uuid)
        |select id, address_id, customer_id, (select * from product_sum), creation_time
        |from orders
        |where customer_id = ${userId.value}::uuid
        |""".stripMargin.query[Order]

    def create(newOrder: NewOrder): doobie.Update0 =
      sql"""insert into orders(address_id, customer_id, total_price, creation_time)
           values (
           ${newOrder.addressId},xxx
           ${newOrder.customerId.value}::uuid,
           ${newOrder.totalPrice},
           ${LocalDateTime.now()})""".update

    def findById(orderId: OrderId): doobie.Query0[Order] =
      sql"""with product_sum as (select coalesce(sum(p.price * order_items.quantity), 0) as total_price
        |                     from order_items
        |                     inner join products p on p.id = order_items.product_id
        |                     where order_id = ${orderId.value})
        |select id, address_id, customer_id, (select * from product_sum), creation_time
        |from orders
        |where id = ${orderId.value}
        |""".stripMargin.query[Order]

    def update(order: Order): doobie.Update0 =
      sql"""update orders
            set address_id = ${order.addressId},
            customer_id = ${order.customerId.value}::uuid,
            total_price = ${order.totalPrice},
            creation_time = ${order.creationTime}
            where id = ${order.id.value}""".update

    def delete(orderId: OrderId): doobie.Update0 =
      sql"delete from orders where id = ${orderId.value}".update

    def deleteFromOrderItems(orderId: OrderId): doobie.Update0 =
      sql"delete from order_items where order_id = ${orderId.value}".update
  }

  private final class Impl extends OrderSql {
    override def create(order: NewOrder): doobie.ConnectionIO[Either[SQLException, Order]] =
      sqls
        .create(order)
        .withUniqueGeneratedKeys[OrderId]("id")
        .attemptSql
        .map(_.map(NewOrder.toOrder(_, order)))

    override def findById(orderId: OrderId): doobie.ConnectionIO[Option[Order]] =
      sqls.findById(orderId).option

    override def update(order: Order): doobie.ConnectionIO[Either[SQLException, Order]] =
      sqls
        .update(order)
        .run
        .attemptSql
        .map(_.map(_ => order))

    override def delete(orderId: OrderId): doobie.ConnectionIO[Either[SQLException, Int]] =
      (for {
        _ <- sqls.deleteFromOrderItems(orderId).run
        res <- sqls.delete(orderId).run
      } yield res).attemptSql

    override def findByUserId(userId: UserId): doobie.ConnectionIO[Either[SQLException, List[Order]]] =
      sqls.findByUserId(userId).to[List].attemptSql

    override def findBySellerId(userId: UserId): doobie.ConnectionIO[Either[SQLException, List[Order]]] =
      sqls.findBySellerId(userId).to[List].attemptSql

  }

  def make: OrderSql = new Impl
}
