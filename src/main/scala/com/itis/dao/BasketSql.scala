package com.itis.dao

import com.itis.domain.types.BasketId
import com.itis.domain.{Basket, BasketUpdate, NewBasket}
import doobie._
import doobie.implicits._
import doobie.postgres.implicits._

import java.sql.SQLException
import java.time.LocalDateTime
import scala.tools.nsc.interpreter.Naming.sessionNames.res

trait BasketSql {

  def createBasket(newBasket: NewBasket): ConnectionIO[Either[SQLException, Basket]]

  def findById(basketId: BasketId): ConnectionIO[Option[Basket]]

  def update(basket: BasketUpdate): doobie.ConnectionIO[Basket]

  def deleteBasket(basketId: BasketId): ConnectionIO[Either[SQLException, Int]]

  def updateTime(basketId: BasketId): doobie.ConnectionIO[Either[SQLException, Int]]
}

object BasketSql {
  private object sqls {
    private val select = {
      fr"""select
          |id,
          |customer_id,
          |total_price,
          |update_time
          |from baskets""".stripMargin
    }

    def createBasket(newBasket: NewBasket): doobie.Update0 =
      sql"""insert into baskets(customer_id, update_time)
           |values (
           |${newBasket.customerId.value}::uuid,
           |${LocalDateTime.now()}
           |)""".stripMargin.update

    def findById(basketId: BasketId): doobie.Query0[Basket] =
      sql"""
           |with product_sum as (select coalesce(sum(p.price * product_basket.quantity), 0) as total_price
           |                     from product_basket
           |                              inner join products p on p.id = product_basket.product_id
           |                     where product_basket.basket_id = ${basketId.value})
           |select id, customer_id, (select * from product_sum), update_time
           |from baskets
           |where baskets.id = ${basketId.value}
           |""".stripMargin.query[Basket]

    def updateTime(basketId: BasketId): doobie.Update0 =
      sql"""update baskets
            set update_time = ${LocalDateTime.now()}
            where id = ${basketId.value}""".update

    def update(basket: BasketUpdate): doobie.Update0 =
      sql"""update baskets
           |set customer_id = ${basket.customerId.value}::uuid,
           |update_time = ${LocalDateTime.now()}
           |where id = ${basket.id.value}""".stripMargin.update

    def deleteFromProductBasket(basketId: BasketId): doobie.Update0 =
      sql"delete from product_basket where basket_id = ${basketId.value}".update

    def deleteBasket(basketId: BasketId): doobie.Update0 =
      sql"delete from baskets where id = ${basketId.value}".update
  }

  private final class Impl extends BasketSql {
    override def createBasket(newBasket: NewBasket): doobie.ConnectionIO[Either[SQLException, Basket]] =
      sqls
        .createBasket(newBasket)
        .withUniqueGeneratedKeys[BasketId]("id")
        .attemptSql
        .map(_.map(Basket(_, newBasket.customerId, 0, LocalDateTime.now())))

    override def findById(basketId: BasketId): doobie.ConnectionIO[Option[Basket]] =
      sqls.findById(basketId).option

    override def update(basket: BasketUpdate): doobie.ConnectionIO[Basket] = {
      sqls.update(basket).run
      sqls.findById(basket.id).unique
    }

    override def deleteBasket(basketId: BasketId): doobie.ConnectionIO[Either[SQLException, Int]] =
      (for {
        _ <- sqls.deleteFromProductBasket(basketId).run
        deletedRows <- sqls.deleteBasket(basketId).run
      } yield deletedRows).attemptSql

    override def updateTime(basketId: BasketId): ConnectionIO[Either[SQLException, Int]] =
      sqls
        .updateTime(basketId)
        .run
        .attemptSql

  }
  def make: BasketSql = new Impl

}
