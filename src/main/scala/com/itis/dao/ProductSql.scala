package com.itis.dao

import com.itis.domain.types._
import com.itis.domain.{NewProduct, Product, ProductFilter}
import doobie.{Fragment, Fragments, _}
import doobie.implicits._
import doobie.postgres.implicits._
import doobie._

import java.sql.SQLException
import java.time.LocalDateTime

trait ProductSql {
  def filterProducts(filter: ProductFilter): ConnectionIO[Either[SQLException, List[Product]]]

  def findAll(): ConnectionIO[Either[SQLException, List[Product]]]

  def findBySellerId(userId: UserId): ConnectionIO[Either[SQLException, List[Product]]]

  def findByCategoryId(categoryId: CategoryId): ConnectionIO[Either[SQLException, List[Product]]]

  def createProduct(product: NewProduct): ConnectionIO[Either[SQLException, Product]]

  def findProductById(productId: ProductId): ConnectionIO[Option[Product]]

  def updateProduct(product: Product): ConnectionIO[Either[SQLException, Product]]

  def deleteProduct(productId: ProductId): ConnectionIO[Either[SQLException, Int]]

}

object ProductSql {

  private final class Impl extends ProductSql {
    override def createProduct(product: NewProduct): doobie.ConnectionIO[Either[SQLException, Product]] =
      sqls
        .insertProduct(product)
        .withUniqueGeneratedKeys[ProductId]("id")
        .attemptSql
        .map(_.map(NewProduct.toProduct(_, product)))

    override def findProductById(productId: ProductId): doobie.ConnectionIO[Option[Product]] =
      sqls.findById(productId).option

    override def updateProduct(product: Product): doobie.ConnectionIO[Either[SQLException, Product]] =
      sqls
        .updateProduct(product)
        .run
        .attemptSql
        .map(_.map(_ => product))

    override def deleteProduct(productId: ProductId): doobie.ConnectionIO[Either[SQLException, Int]] =
      (for {
        _ <- sqls.deleteProductFromOrderItems(productId).run
        _ <- sqls.deleteProductFromFiles(productId).run
        _ <- sqls.deleteProductFromProductBasket(productId).run
        deletedRows <- sqls.deleteProduct(productId).run
      } yield deletedRows).attemptSql

    override def findByCategoryId(categoryId: CategoryId): doobie.ConnectionIO[Either[SQLException, List[Product]]] =
      sqls
        .findByCategoryId(categoryId)
        .to[List]
        .attemptSql

    override def findBySellerId(userId: UserId): doobie.ConnectionIO[Either[SQLException, List[Product]]] =
      sqls
        .findBySellerId(userId)
        .to[List]
        .attemptSql

    override def findAll(): doobie.ConnectionIO[Either[SQLException, List[Product]]] =
      sqls
        .findAll()
        .to[List]
        .attemptSql

    override def filterProducts(filter: ProductFilter): doobie.ConnectionIO[Either[SQLException, List[Product]]] = {
      sqls
        .filterProducts(filter)
        .to[List]
        .attemptSql
    }
  }

  private object sqls {
    def filterProducts(filter: ProductFilter): doobie.Query0[Product] =
      (select ++ Fragments.whereAndOpt(
        filter.name.map(name => fr"title like '%' || $name || '%'"),
        filter.priceLow.map(low => fr"price > $low"),
        filter.priceHigh.map(high => fr"price < $high"),
        filter.category.map(category => fr"category_id = ${category.value}")
      )).query[Product]

    def findAll(): doobie.Query0[Product] = select.query[Product]

    def findByCategoryId(categoryId: CategoryId): doobie.Query0[Product] =
      (select ++ fr"where category_id = ${categoryId.value}").query[Product]

    def findBySellerId(userId: UserId): doobie.Query0[Product] =
      (select ++ fr"where seller_id::text = ${userId.value}").query[Product]

    private val select: Fragment =
      fr"""select id,
           |       title,
           |       seller_id,
           |       price,
           |       category_id,
           |       quantity,
           |       description,
           |       creation_time
           |from products
           |""".stripMargin

    def insertProduct(newProduct: NewProduct): doobie.Update0 =
      sql"""insert into products (title, seller_id, price, category_id, quantity, description, creation_time)
            values (${newProduct.title},
           |${newProduct.sellerId.value}::uuid,
           |${newProduct.price},
           |${newProduct.categoryId.value},
           |${newProduct.quantity},
           |${newProduct.description},
           |${LocalDateTime.now()})
        """.stripMargin.update

    def findById(productId: ProductId): doobie.Query0[Product] =
      (select ++ fr"where id = ${productId.value}").query[Product]

    def updateProduct(product: Product): doobie.Update0 =
      sql"""update products
           |set title = ${product.title},
           |seller_id = ${product.sellerId.value}::uuid,
           |price = ${product.price},
           |category_id = ${product.categoryId.value},
           |quantity = ${product.quantity},
           |description = ${product.description},
           |creation_time = ${product.creationTime}
           |where id = ${product.id.value}
          """.stripMargin.update

    def deleteProductFromFiles(productId: ProductId): doobie.Update0 =
      sql"""delete from product_files where product_id = ${productId.value}""".update

    def deleteProductFromOrderItems(productId: ProductId): doobie.Update0 =
      sql"""delete from order_items where product_id = ${productId.value}""".update

    def deleteProductFromProductBasket(productId: ProductId): doobie.Update0 =
      sql"""delete from product_basket where product_id = ${productId.value}""".update

    def deleteProduct(productId: ProductId): doobie.Update0 =
      sql"""delete from products where id = ${productId.value}""".update
  }

  def make: ProductSql = new Impl
}
