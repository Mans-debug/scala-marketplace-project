package com.itis.dao

import com.itis.domain.{Address, NewAddress}
import doobie._
import doobie.implicits._

import java.sql.SQLException

trait AddressSql {

  def createAddress(address: NewAddress): ConnectionIO[Either[SQLException, Address]]

  def findById(addressId: Long): ConnectionIO[Option[Address]]

  def update(address: Address): ConnectionIO[Either[SQLException, Address]]

  def deleteAddress(addressId: Long): ConnectionIO[Either[SQLException, Int]]

}

object AddressSql {
  private object sqls {
    private val select = sql"select id, full_address from addresses"

    def insertAddress(newAddress: NewAddress): doobie.Update0 =
      sql"""insert into addresses(full_address)
           values (${newAddress.fullAddress})""".update

    def findById(addressId: Long): doobie.Query0[Address] =
      (select ++ sql"where id = $addressId").query[Address]

    def updateAddress(address: Address): doobie.Update0 =
      sql"""update addresses
           |set full_address = ${address.fullAddress}
           |where id = ${address.id}
            """.stripMargin.update

    def deleteAddressFromOrders(addressId: Long): doobie.Update0 =
      sql"""delete from orders where address_id = $addressId""".update

    def deleteAddress(addressId: Long): doobie.Update0 =
      sql"""delete from addresses where id = $addressId""".update
  }

  private final class Impl extends AddressSql {
    override def createAddress(address: NewAddress): doobie.ConnectionIO[Either[SQLException, Address]] =
      sqls
        .insertAddress(address)
        .withUniqueGeneratedKeys[Long]("id")
        .attemptSql
        .map(_.map(Address(_, address.fullAddress)))

    override def findById(addressId: Long): doobie.ConnectionIO[Option[Address]] =
      sqls.findById(addressId).option

    override def update(address: Address): doobie.ConnectionIO[Either[SQLException, Address]] =
      sqls
        .updateAddress(address)
        .run
        .attemptSql
        .map(_.map(_ => address))

    override def deleteAddress(addressId: Long): doobie.ConnectionIO[Either[SQLException, Int]] = {
      (for {
        _ <- sqls.deleteAddressFromOrders(addressId).run
        deletedRows <- sqls.deleteAddress(addressId).run
      } yield deletedRows).attemptSql
    }
  }

  def make: AddressSql = new Impl
}
