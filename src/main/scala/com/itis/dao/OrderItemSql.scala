package com.itis.dao

import com.itis.dao.CategorySql.Impl
import com.itis.domain.{NewOrderItem, OrderItem}
import doobie._
import doobie.implicits._
import doobie.postgres.implicits._
import com.itis.domain.types._

import java.sql.SQLException

trait OrderItemSql {
  def batchCreate(items: List[NewOrderItem]): ConnectionIO[Either[SQLException, List[OrderItem]]]

  def findByOrderId(orderId: OrderId): ConnectionIO[Either[SQLException, List[OrderItem]]]

  def create(newOrderItem: NewOrderItem): ConnectionIO[Either[SQLException, OrderItem]]

  def findById(orderItemId: Long): ConnectionIO[Option[OrderItem]]

  def update(orderItem: OrderItem): ConnectionIO[Either[SQLException, OrderItem]]

  def delete(orderItemId: Long): ConnectionIO[Either[SQLException, Int]]

}

object OrderItemSql {
  private object sqls {
    private val select = sql"""select id, product_id, order_id, status_id, quantity, delivery_time from order_items"""

    def create(newOrderItem: NewOrderItem): doobie.Update0 =
      sql"""insert into order_items(product_id, order_id, status_id, quantity, delivery_time)
           values (
           ${newOrderItem.productId.value},
           ${newOrderItem.orderId.value},
           ${newOrderItem.statusId},
           ${newOrderItem.quantity},
           ${newOrderItem.deliveryTime}
           )""".update

    def findById(orderItemId: Long): doobie.Query0[OrderItem] =
      (select ++ sql"where id = $orderItemId").query[OrderItem]

    def findByOrderId(orderId: OrderId): doobie.Query0[OrderItem] =
      (select ++ sql"where order_id = ${orderId.value}").query[OrderItem]

    def update(orderItem: OrderItem): doobie.Update0 =
      sql"""update order_items
           set product_id = ${orderItem.productId.value},
           order_id = ${orderItem.orderId.value},
           status_id = ${orderItem.statusId},
           quantity = ${orderItem.quantity},
           delivery_time = ${orderItem.deliveryTime}
           where id = ${orderItem.id}""".update

    def delete(orderItemId: Long): doobie.Update0 =
      sql"delete from order_items where id = $orderItemId".update
  }

  private final class Impl extends OrderItemSql {
    override def create(newOrderItem: NewOrderItem): doobie.ConnectionIO[Either[SQLException, OrderItem]] =
      sqls
        .create(newOrderItem)
        .withUniqueGeneratedKeys[Long]("id")
        .attemptSql
        .map(_.map(NewOrderItem.toOrderItem(_, newOrderItem)))

    override def findById(orderItemId: Long): doobie.ConnectionIO[Option[OrderItem]] =
      sqls.findById(orderItemId).option

    override def update(orderItem: OrderItem): doobie.ConnectionIO[Either[SQLException, OrderItem]] =
      sqls
        .update(orderItem)
        .run
        .attemptSql
        .map(_.map(_ => orderItem))

    override def delete(orderItemId: Long): doobie.ConnectionIO[Either[SQLException, Int]] =
      sqls.delete(orderItemId).run.attemptSql

    override def findByOrderId(orderId: OrderId): doobie.ConnectionIO[Either[SQLException, List[OrderItem]]] =
      sqls.findByOrderId(orderId).to[List].attemptSql

    override def batchCreate(items: List[NewOrderItem]): doobie.ConnectionIO[Either[SQLException, List[OrderItem]]] = ???
  }

  def make: OrderItemSql = new Impl
}
