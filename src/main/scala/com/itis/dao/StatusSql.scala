package com.itis.dao

import com.itis.dao.CategorySql.Impl
import com.itis.domain.{NewStatus, Status}
import doobie._
import doobie.implicits._

import java.sql.SQLException
trait StatusSql {
  def findAll(): ConnectionIO[Either[SQLException, List[Status]]]

  def create(newStatus: NewStatus): ConnectionIO[Either[SQLException, Status]]

  def findById(statusId: Long): ConnectionIO[Option[Status]]

  def update(status: Status): ConnectionIO[Either[SQLException, Status]]

  def delete(statusId: Long): ConnectionIO[Either[SQLException, Int]]
}
object StatusSql {

  private final class Impl extends StatusSql {
    override def create(newStatus: NewStatus): doobie.ConnectionIO[Either[SQLException, Status]] =
      sqls
        .create(newStatus)
        .withUniqueGeneratedKeys[Long]("id")
        .attemptSql
        .map(_.map(Status(_, newStatus.name)))

    override def findById(statusId: Long): doobie.ConnectionIO[Option[Status]] =
      sqls.findById(statusId).option

    override def update(status: Status): doobie.ConnectionIO[Either[SQLException, Status]] =
      sqls
        .update(status)
        .run
        .attemptSql
        .map(_.map(_ => status))

    override def delete(statusId: Long): doobie.ConnectionIO[Either[SQLException, Int]] =
      sqls.delete(statusId).run.attemptSql

    override def findAll(): doobie.ConnectionIO[Either[SQLException, List[Status]]] =
      sqls.findAll().to[List].attemptSql
  }
  private object sqls {
    def findAll(): doobie.Query0[Status] = sql"select id, name from statuses".query[Status]

    def create(newStatus: NewStatus): doobie.Update0 =
      sql"insert into statuses(name) values (${newStatus.name})".update

    def findById(statusId: Long): doobie.Query0[Status] =
      sql"select id, name from statuses where id = $statusId".query[Status]

    def update(status: Status): doobie.Update0 =
      sql"update statuses set name = ${status.name} where id = ${status.id}".update

    def delete(statusId: Long): doobie.Update0 =
      sql"delete from statuses where id = $statusId".update
  }

  def make: StatusSql = new Impl
}
