package com.itis.dao

import cats.implicits._
import com.itis.domain.errors.{AppError, UserAlreadyExists, UserNotFound, InternalError}
import com.itis.domain.types._
import com.itis.domain.{CreateUser, User}
import doobie._
import doobie.implicits._
import doobie.postgres.implicits._

trait UserSql {

  def createUser(createUser: CreateUser): doobie.ConnectionIO[Either[AppError, User]]

  def updateUser(user: User): doobie.ConnectionIO[Either[UserNotFound, User]]

  def getUser(uuid: UserId): doobie.ConnectionIO[Option[User]]

  def deleteUser(uuid: UserId): doobie.ConnectionIO[Either[UserNotFound, Int]]

  def findUserByPhone(phone: Phone): ConnectionIO[Option[User]]

  def findUserByEmail(email: Email): ConnectionIO[Option[User]]

  def userList: ConnectionIO[List[User]]
}

object UserSql {
  object sqls {

    private val selectFields =
      fr"""select id,
             user_role,
             email,
             name,
             surname,
             patronymic,
             password_hash,
             gender,
             birth_date,
             phone,
             profile_picture,
             points,
             city
            from users"""
    def insertSql(createUser: CreateUser): doobie.Update0 =
      sql"""insert into users (name,
             user_role,
             surname,
             patronymic,
             password_hash,
             gender,
             birth_date,
             phone,
             email)
            values (${createUser.name},
                    ${createUser.role.name},
                    ${createUser.surname},
                    ${createUser.patronymic},
                    ${createUser.passwordHash.value},
                    ${createUser.gender},
                    ${createUser.birthDate}::timestamp,
                    ${createUser.phone.value},
                    ${createUser.email.value})
        """.update

    def updateSql(user: User): doobie.Update0 =
      sql"""update users u
           set name = ${user.name},
           surname = ${user.surname},
           patronymic = ${user.patronymic},
           gender = ${user.gender},
           birth_date = ${user.birthDate},
           email = ${user.email.value},
           user_role = ${user.role.name},
           phone = ${user.phone.value},
           profile_picture = ${user.profilePicture}::uuid,
           points = ${user.points},
           city = ${user.city}
        where id::text = ${user.id.value};
        """.update

    def findByIdSql(uuid: String): doobie.Query0[User] = (selectFields ++ fr"where id::text = $uuid").query[User]

    def deleteSql(uuid: String): doobie.Update0 = sql"delete from users where id::text = $uuid".update

    val findAllSql: doobie.Query0[User] = sql"select * from users".query[User]

    def findByPhone(phone: Phone): doobie.Query0[User] = (selectFields ++ fr"where phone = ${phone.value}").query[User]

    def findByEmail(email: Email): doobie.Query0[User] = (selectFields ++ fr"where email = ${email.value}").query[User]
  }

  private final class Impl extends UserSql {

    import sqls._

    override def getUser(uuid: UserId): doobie.ConnectionIO[Option[User]] = findByIdSql(uuid.value).option

    override def deleteUser(uuid: UserId): doobie.ConnectionIO[Either[UserNotFound, Int]] =
      deleteSql(uuid.value).run.map {
        case 0           => UserNotFound(uuid.value).asLeft[Int]
        case deletedRows => deletedRows.asRight[UserNotFound]
      }

    override def userList: doobie.ConnectionIO[List[User]] = findAllSql.to[List]

    override def createUser(createUser: CreateUser): doobie.ConnectionIO[Either[AppError, User]] =
      insertSql(createUser)
        .withUniqueGeneratedKeys[UserId]("id")
        .attempt
        .map {
          case Right(userId) => CreateUser.toUser(userId, createUser).asRight[AppError]
          case Left(ex) => InternalError(ex).asLeft[User]
        }

    override def updateUser(user: User): doobie.ConnectionIO[Either[UserNotFound, User]] =
      sqls
        .updateSql(user)
        .run
        .map {
          case 0 => UserNotFound(user.id.value).asLeft[User]
          case _ => user.asRight[UserNotFound]
        }

    override def findUserByPhone(phone: Phone): doobie.ConnectionIO[Option[User]] = sqls.findByPhone(phone).option

    override def findUserByEmail(email: Email): doobie.ConnectionIO[Option[User]] = sqls.findByEmail(email).option
  }

  def make: UserSql = new Impl
}
