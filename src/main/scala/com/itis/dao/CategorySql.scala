package com.itis.dao

import cats.implicits.catsSyntaxEitherId
import com.itis.dao.AddressSql.Impl
import com.itis.domain.{Category, NewCategory, errors}
import com.itis.domain.errors.AppError
import com.itis.domain.types._
import com.itis.domain.types.CategoryId._
import doobie._
import doobie.implicits._
import monocle.syntax.all._

import java.sql.SQLException

trait CategorySql {
  def findAll(): ConnectionIO[Either[SQLException, List[Category]]]

  def createCategory(category: NewCategory): ConnectionIO[Either[AppError, Category]]

  def findCategoryById(categoryId: CategoryId): ConnectionIO[Option[Category]]

  def updateCategory(category: Category): ConnectionIO[Either[AppError, Category]]

  def deleteCategory(categoryId: CategoryId): ConnectionIO[Either[AppError, Int]]
}

object CategorySql {
  private object sqls {

    val select = fr"select id, parent_id, title from categories"

    def insertCategory(category: NewCategory): doobie.Update0 =
      sql"""insert into categories (parent_id, title)
           values (${category.parentId.map(_.value)}, ${category.title})""".update

    def findCategoryById(categoryId: CategoryId): doobie.Query0[Category] =
      (select ++ fr"where id = ${categoryId.value}").query[Category]

    def findAll(): doobie.Query0[Category] =
      select.query[Category]

    def updateCategory(category: Category): doobie.Update0 =
      sql"""update categories
           set title = ${category.title},
           parent_id = ${category.parentId.map(_.value)},
           where id = ${category.id.value}
           """.update

    def deleteCategory(categoryId: CategoryId): doobie.Update0 =
      sql"delete from categories where parent_id = ${categoryId.value} or id = ${categoryId.value}".update

  }

  private final class Impl extends CategorySql {
    override def createCategory(category: NewCategory): doobie.ConnectionIO[Either[AppError, Category]] =
      sqls
        .insertCategory(category)
        .withUniqueGeneratedKeys[CategoryId]("id")
        .attemptSql
        .map {
          case Right(categoryId) => NewCategory.toCategory(categoryId, category).asRight[AppError]
          case Left(sqlError)    => errors.InternalError(sqlError).asLeft[Category]
        }

    override def findCategoryById(categoryId: CategoryId): doobie.ConnectionIO[Option[Category]] =
      sqls.findCategoryById(categoryId).option

    override def updateCategory(category: Category): doobie.ConnectionIO[Either[AppError, Category]] =
      sqls
        .updateCategory(category)
        .run
        .attemptSql
        .map {
          case Right(_)       => category.asRight[AppError]
          case Left(sqlError) => errors.InternalError(sqlError).asLeft[Category]
        }

    override def deleteCategory(categoryId: CategoryId): doobie.ConnectionIO[Either[AppError, Int]] =
      sqls
        .deleteCategory(categoryId)
        .run
        .attemptSql
        .map {
          case Right(deletedRows) => deletedRows.asRight[AppError]
          case Left(sqlError)     => errors.InternalError(sqlError).asLeft[Int]
        }

    override def findAll(): doobie.ConnectionIO[Either[SQLException, List[Category]]] =
      sqls
        .findAll()
        .to[List]
        .attemptSql

  }

  def make: CategorySql = new Impl
}
