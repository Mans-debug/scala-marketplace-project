package com.itis.dao

import com.itis.domain.types.BasketId
import com.itis.domain.{NewProductBasket, ProductBasket, types}
import doobie._
import doobie.implicits._

import java.sql.SQLException

trait ProductBasketSql {
  def findByBasketId(basketId: BasketId): ConnectionIO[Either[SQLException, List[ProductBasket]]]

  def create(newProductBasket: NewProductBasket): ConnectionIO[Either[SQLException, ProductBasket]]

  def findById(productBasketId: Long): ConnectionIO[Option[ProductBasket]]

  def update(productBasket: ProductBasket): ConnectionIO[Either[SQLException, ProductBasket]]

  def delete(productBasketId: Long): ConnectionIO[Either[SQLException, Int]]

}

object ProductBasketSql {
  private object sqls {
    private val select = fr"select id, product_id, basket_id, quantity from product_basket"

    def create(basket: NewProductBasket): doobie.Update0 =
      sql"""insert into product_basket(product_id, basket_id, quantity)
           values (
           ${basket.productId.value},
           ${basket.basketId.value},
           ${basket.quantity}
           )""".update

    def findByBasketId(basketId: BasketId): doobie.Query0[ProductBasket] =
      (select ++ sql"where basket_id = ${basketId.value}").query[ProductBasket]

    def findById(productBasketId: Long): doobie.Query0[ProductBasket] =
      (select ++ sql"where id = $productBasketId").query[ProductBasket]

    def update(productBasket: ProductBasket): doobie.Update0 =
      sql"""update product_basket
           set product_id = ${productBasket.productId.value},
           basket_id = ${productBasket.basketId.value},
           quantity = ${productBasket.quantity}
           where id = ${productBasket.id}""".update

    def delete(productBasketId: Long): doobie.Update0 =
      sql"delete from product_basket where id = $productBasketId".update
  }

  private final class Impl extends ProductBasketSql {
    override def create(newProductBasket: NewProductBasket): doobie.ConnectionIO[Either[SQLException, ProductBasket]] =
      sqls
        .create(newProductBasket)
        .withUniqueGeneratedKeys[Long]("id")
        .attemptSql
        .map(_.map(NewProductBasket.toProductBasket(_, newProductBasket)))

    override def findById(productBasketId: Long): doobie.ConnectionIO[Option[ProductBasket]] =
      sqls.findById(productBasketId).option

    override def update(productBasket: ProductBasket): doobie.ConnectionIO[Either[SQLException, ProductBasket]] =
      sqls
        .update(productBasket)
        .run
        .attemptSql
        .map(_.map(_ => productBasket))

    override def delete(productBasketId: Long): doobie.ConnectionIO[Either[SQLException, Int]] =
      sqls.delete(productBasketId).run.attemptSql

    override def findByBasketId(basketId: BasketId): doobie.ConnectionIO[Either[SQLException, List[ProductBasket]]] =
      sqls
        .findByBasketId(basketId)
        .to[List]
        .attemptSql
  }

  def make: ProductBasketSql = new Impl
}
