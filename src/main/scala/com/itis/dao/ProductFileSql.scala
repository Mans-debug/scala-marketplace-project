package com.itis.dao

import com.itis.dao.CategorySql.Impl
import com.itis.domain.ProductFile
import com.itis.domain.types.{ProductId, FileId}
import doobie._
import doobie.implicits._

import java.sql.SQLException

trait ProductFileSql {
  def create(productFiles: List[ProductFile]): ConnectionIO[Either[SQLException, List[ProductFile]]]

  def findByProductId(productId: ProductId): ConnectionIO[Either[SQLException, List[ProductFile]]]

  def delete(productId: ProductId): ConnectionIO[Either[SQLException, Int]]

}

object ProductFileSql {
  private object sqls {
    def insert(productFiles: List[ProductFile]): doobie.ConnectionIO[Int] = {
      val sql = sql"insert into product_files(product_id, file_id) values (?, ?)".stripMargin
      Update[ProductFile](sql.toString()).updateMany(productFiles)
    }

    def findByProductId(productId: ProductId): doobie.ConnectionIO[List[ProductFile]] =
      sql"select product_id, file_id from product_files where product_id = ${productId.value}"
        .query[ProductFile]
        .to[List]

    def deleteProductFiles(productId: ProductId): doobie.Update0 =
      sql"delete from product_files where product_id = ${productId.value}".update
  }

  private final class Impl extends ProductFileSql {
    override def create(productFiles: List[ProductFile]): doobie.ConnectionIO[Either[SQLException, List[ProductFile]]] =
      sqls
        .insert(productFiles)
        .attemptSql
        .map(_.map(_ => productFiles))

    override def findByProductId(productId: ProductId): doobie.ConnectionIO[Either[SQLException, List[ProductFile]]] = {
      sqls.findByProductId(productId).attemptSql

    }

    override def delete(productId: ProductId): doobie.ConnectionIO[Either[SQLException, Int]] =
      sqls.deleteProductFiles(productId).run.attemptSql
  }

  def make: ProductFileSql = new Impl
}
