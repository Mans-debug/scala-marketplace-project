package com.itis.service

import cats.effect.IO
import com.itis.controller.endpoints.category
import com.itis.dao.CategorySql
import com.itis.domain.errors.{AppError, InternalError}
import com.itis.domain.types.CategoryId
import com.itis.domain.{Category, NewCategory, SimpleMessage}
import doobie.Transactor
import doobie.implicits._
trait CategoryService {
  def findAll(): IO[Either[AppError, List[Category]]]
  def createCategory(category: NewCategory): IO[Either[AppError, Category]]

  def findByCategoryId(categoryId: CategoryId): IO[Either[AppError, Option[Category]]]

  def updateCategory(category: Category): IO[Either[AppError, Category]]

  def deleteCategory(categoryId: CategoryId): IO[Either[AppError, SimpleMessage]]

}

object CategoryService {
  private final class Impl(sql: CategorySql, transactor: Transactor[IO]) extends CategoryService {
    override def createCategory(category: NewCategory): IO[Either[AppError, Category]] =
      sql
        .createCategory(category)
        .transact[IO](transactor)

    override def findByCategoryId(categoryId: CategoryId): IO[Either[AppError, Option[Category]]] =
      sql
        .findCategoryById(categoryId)
        .transact[IO](transactor)
        .attempt
        .map(_.left.map(InternalError(_)))

    override def updateCategory(category: Category): IO[Either[AppError, Category]] =
      sql
        .updateCategory(category: Category)
        .transact[IO](transactor)

    override def deleteCategory(categoryId: CategoryId): IO[Either[AppError, SimpleMessage]] =
      sql
        .deleteCategory(categoryId)
        .transact[IO](transactor)
        .map(_.map(deleted => SimpleMessage(s"Deleted $deleted rows")))

    override def findAll(): IO[Either[AppError, List[Category]]] =
      sql
        .findAll()
        .transact(transactor)
        .map(_.left.map(InternalError(_)))

  }

  def make(sql: CategorySql, transactor: Transactor[IO]): CategoryService = new Impl(sql, transactor)
}
