package com.itis.service

import cats.effect.IO
import com.itis.dao.BasketSql
import com.itis.domain.errors.{AppError, InternalError}
import com.itis.domain.types.BasketId
import com.itis.domain.{Basket, BasketUpdate, NewBasket, SimpleMessage}
import doobie.Transactor
import doobie.implicits._

trait BasketService {
  def createBasket(newBasket: NewBasket): IO[Either[AppError, Basket]]

  def findById(basketId: BasketId): IO[Either[AppError, Option[Basket]]]

  def update(basket: BasketUpdate): IO[Either[AppError, Basket]]

  def deleteBasket(basketId: BasketId): IO[Either[AppError, SimpleMessage]]

  def updateTime(basketId: BasketId): IO[Either[AppError, Int]]
}

object BasketService {
  private final class Impl(sql: BasketSql, transactor: Transactor[IO]) extends BasketService {
    override def createBasket(newBasket: NewBasket): IO[Either[AppError, Basket]] =
      sql
        .createBasket(newBasket)
        .transact[IO](transactor)
        .map(_.left.map(InternalError(_)))

    override def findById(basketId: BasketId): IO[Either[AppError, Option[Basket]]] =
      sql
        .findById(basketId)
        .transact[IO](transactor)
        .attempt
        .map(_.left.map(InternalError(_)))

    override def update(basket: BasketUpdate): IO[Either[AppError, Basket]] =
      sql
        .update(basket)
        .transact[IO](transactor)
        .attempt
        .map(_.left.map(InternalError(_)))

    override def deleteBasket(basketId: BasketId): IO[Either[AppError, SimpleMessage]] =
      sql
        .deleteBasket(basketId)
        .transact[IO](transactor)
        .map(_.left.map(InternalError(_)).map(d => SimpleMessage(s"Deleted $d rows")))

    override def updateTime(basketId: BasketId): IO[Either[AppError, Int]] =
      sql
        .updateTime(basketId)
        .transact[IO](transactor)
        .map(_.left.map(InternalError(_)))
  }

  def make(sql: BasketSql, transactor: Transactor[IO]): BasketService = new Impl(sql, transactor)
}
