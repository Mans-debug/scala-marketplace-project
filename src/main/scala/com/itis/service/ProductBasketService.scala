package com.itis.service

import cats.effect.IO
import com.itis.dao.ProductBasketSql
import com.itis.domain.errors.{AppError, InternalError}
import com.itis.domain.types.BasketId
import com.itis.domain.{NewProductBasket, ProductBasket, SimpleMessage}
import doobie.implicits._
import doobie.util.transactor.Transactor

trait ProductBasketService {
  def findByBasketId(basketId: BasketId): IO[Either[AppError, List[ProductBasket]]]


  def create(newProductBasket: NewProductBasket): IO[Either[AppError, ProductBasket]]

  def findById(productBasketId: Long): IO[Either[AppError, Option[ProductBasket]]]

  def update(productBasket: ProductBasket): IO[Either[AppError, ProductBasket]]

  def delete(productBasketId: Long): IO[Either[AppError, SimpleMessage]]
}

object ProductBasketService {
  private final class Impl(sql: ProductBasketSql, transator: Transactor[IO], basketService: BasketService) extends ProductBasketService {
    override def create(newProductBasket: NewProductBasket): IO[Either[AppError, ProductBasket]] =
      for {
        update <- basketService.updateTime(newProductBasket.basketId)
        basketItem <- sql
          .create(newProductBasket)
          .transact(transator)
          .map(_.left.map(InternalError(_)))
      } yield basketItem

    override def findById(productBasketId: Long): IO[Either[AppError, Option[ProductBasket]]] =
      for {
        find <- sql
          .findById(productBasketId)
          .transact(transator)
          .attempt
          .map(_.left.map(InternalError(_)))
      } yield find

    override def update(productBasket: ProductBasket): IO[Either[AppError, ProductBasket]] =
      for {
        _ <- basketService.updateTime(productBasket.basketId)
        updateItem <- sql
          .update(productBasket)
          .transact(transator)
          .map(_.left.map(InternalError(_)))
      } yield updateItem

    override def delete(productBasketId: Long): IO[Either[AppError, SimpleMessage]] =
      for {
        _ <- basketService.updateTime(BasketId(productBasketId))
        delete <- sql
          .delete(productBasketId)
          .transact(transator)
          .map(_.left.map(InternalError(_)).map(d => SimpleMessage(s"Deleted $d rows")))
      } yield delete

    override def findByBasketId(basketId: BasketId): IO[Either[AppError, List[ProductBasket]]] =
      sql
        .findByBasketId(basketId)
        .transact(transator)
        .map(_.left.map(InternalError(_)))
  }

  def make(sql: ProductBasketSql,
           transactor: Transactor[IO],
           basketService: BasketService): ProductBasketService =
    new Impl(sql, transactor, basketService)
}
