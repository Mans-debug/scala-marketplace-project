package com.itis.service

trait Converter[A, B] {
  def convert(a: A): B
}
