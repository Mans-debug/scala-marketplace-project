package com.itis.service

import cats.effect.IO
import cats.implicits._
import com.itis.dao.UserSql
import com.itis.domain.errors.{AppError, InternalError}
import com.itis.domain.types.{Salt, UserId}
import com.itis.domain.{CreateUser, SignUpForm, User}
import doobie._
import doobie.implicits._
import org.mindrot.jbcrypt.BCrypt
import tofu.logging.Logging
import tofu.syntax.error

trait UserService {
  def createUser(signUpForm: SignUpForm): IO[Either[AppError, User]]

  def updateUser(user: User): IO[Either[AppError, User]]

  def userById(uuid: UserId): IO[Either[InternalError, Option[User]]]

  def deleteUserById(uuid: UserId): IO[Either[AppError, Int]]

  def findAll: IO[Either[InternalError, List[User]]]
}

object UserService {
  private final class Impl(sql: UserSql, transactor: Transactor[IO])(implicit log: Logging[IO]) extends UserService {
    override def createUser(signUpForm: SignUpForm): IO[Either[AppError, User]] = {
      sql
        .createUser(CreateUser.fromSignUpForm(signUpForm, Salt(BCrypt.gensalt())))
        .transact[IO](transactor)
        .attempt
        .map {
          case Left(error)        => InternalError(error).asLeft[User]
          case Right(Left(error)) => error.asLeft[User]
          case Right(Right(user)) => user.asRight[InternalError]
        }
    }

    override def updateUser(user: User): IO[Either[AppError, User]] =
      sql
        .updateUser(user)
        .transact[IO](transactor)
        .attempt
        .map {
          case Left(error)        => InternalError(error).asLeft[User]
          case Right(Left(error)) => error.asLeft[User]
          case Right(Right(user)) => user.asRight[InternalError]
        }

    override def userById(uuid: UserId): IO[Either[InternalError, Option[User]]] =
      sql
        .getUser(uuid)
        .transact[IO](transactor)
        .attempt
        .map(_.leftMap(th => InternalError(th)))

    override def deleteUserById(uuid: UserId): IO[Either[AppError, Int]] =
      sql
        .deleteUser(uuid)
        .transact[IO](transactor)
        .attempt
        .map {
          case Left(error)               => InternalError(error).asLeft[Int]
          case Right(Left(error))        => error.asLeft[Int]
          case Right(Right(deletedRows)) => deletedRows.asRight[InternalError]
        }

    override def findAll: IO[Either[InternalError, List[User]]] =
      sql.userList
        .transact[IO](transactor)
        .attempt
        .map(_.leftMap(th => InternalError(th)))
  }

  private final class LoggingImpl(impl: Impl)(implicit log: Logging[IO]) extends UserService {
    override def createUser(signUpForm: SignUpForm): IO[Either[AppError, User]] =
      for {
        _ <- log.info(s"Registering new user with phone ${signUpForm.phone}")
        res <- impl.createUser(signUpForm)
        _ <- res match {
          case Left(error)  => log.error(s"Could sign up, got error ${error.toString}")
          case Right(value) => log.info(s"Successfully registered user with id ${value.id}")
        }
      } yield res

    override def updateUser(user: User): IO[Either[AppError, User]] =
      for {
        _ <- log.info(s"Updating user with id ${user.id}")
        res <- impl.updateUser(user)
        _ <- res match {
          case Left(error) => log.error(s"Could not update user with id ${user.id}, got error ${error.toString}")
          case Right(_)    => log.info(s"Successfully updated user with id ${user.id}")
        }
      } yield res

    override def userById(uuid: UserId): IO[Either[InternalError, Option[User]]] =
      for {
        _ <- log.info(s"Fetching user by id $uuid")
        res <- impl.userById(uuid)
        _ <- res match {
          case Left(error) => log.error(s"Could not fetch user by id $uuid, ${error.toString}")
          case Right(user) => log.info(s"Fetched user with id $uuid")
        }
      } yield res

    override def deleteUserById(uuid: UserId): IO[Either[AppError, Int]] =
      for {
        _ <- log.info(s"Deleting user by id $uuid")
        res <- impl.deleteUserById(uuid)
        _ <- res match {
          case Left(error) => log.error(s"Could not delete user by id $uuid, ${error.toString}")
          case Right(rows) => log.info(s"Deleted user with id $uuid, deleted $rows rows")
        }
      } yield res

    override def findAll: IO[Either[InternalError, List[User]]] =
      for {
        _ <- log.info("Fetching all users")
        res <- impl.findAll
      } yield res
  }
  def make(sql: UserSql, transactor: Transactor[IO])(implicit log: Logging[IO]): UserService =
    new LoggingImpl(new Impl(sql, transactor))
}
