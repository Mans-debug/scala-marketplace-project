package com.itis.service

import cats.effect.IO
import cats.implicits.catsSyntaxEitherId
import com.itis.dao.UserSql
import com.itis.domain.errors.{AppError, AuthError, UserNotFound}
import com.itis.domain.{SignInForm, SimpleMessage, User, UserPrincipal, UserPrincipalWithToken}
import doobie.Transactor
import doobie.implicits._
import doobie.util.{log, transactor}
import org.mindrot.jbcrypt.BCrypt
import tofu.logging.Logging

trait AuthService {
  def auth(signInForm: SignInForm): IO[Either[AppError, UserPrincipalWithToken]]
}

object AuthService {
  def make(userSql: UserSql, transactor: Transactor[IO], tokenService: TokenService)(implicit
    log: Logging[IO]
  ): AuthService =
    new Impl(userSql, transactor, tokenService)

  private final class Impl(userSql: UserSql, transactor: Transactor[IO], tokenService: TokenService)(implicit
    log: Logging[IO]
  ) extends AuthService {
    override def auth(signInForm: SignInForm): IO[Either[AppError, UserPrincipalWithToken]] = {
      def isPasswordValid(form: SignInForm, user: User): Boolean = {
        BCrypt.checkpw(form.password.value, user.passwordHash.value)
      }

      def encodeJwt(userIO: IO[Option[User]]): IO[Either[AppError, UserPrincipalWithToken]] = userIO.map {
        case None => UserNotFound("Could not find user for specified creds").asLeft[UserPrincipalWithToken]
        case Some(user) =>
          if (isPasswordValid(signInForm, user)) {
            val token = tokenService.encode(user)
            UserPrincipalWithToken(user.id, user.role, token).asRight[AppError]
          } else {
            AuthError(s"Invalid password for user with id ${user.id}").asLeft[UserPrincipalWithToken]
          }
      }

      log.info(s"Authenticating user with email ${signInForm.email} or phone ${signInForm.phone}")
      (signInForm.email, signInForm.phone) match {
        case (None, None) => IO.pure(AuthError("No login provided").asLeft[UserPrincipalWithToken])
        case (Some(email), _) =>
          val user = userSql.findUserByEmail(email).transact[IO](transactor)
          encodeJwt(user)
        case (None, Some(phone)) =>
          val user = userSql.findUserByPhone(phone).transact[IO](transactor)
          encodeJwt(user)
      }
    }
  }
}
