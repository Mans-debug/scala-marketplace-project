package com.itis.service

import cats.data.{EitherT, OptionT}
import cats.effect.IO
import com.itis.controller.endpoints.order.{create, items}
import com.itis.dao.OrderItemSql
import com.itis.domain.errors.{AppError, InternalError}
import com.itis.domain.types.OrderId
import com.itis.domain.{NewOrderItem, Order, OrderItem, SimpleMessage, types}
import doobie.util.transactor.Transactor
import doobie.implicits._
import monocle.syntax.all._
import org.http4s.CharsetRange.*
import cats.implicits._

trait OrderItemService {
  def batchCreate(orderId: OrderId, newItems: List[NewOrderItem]): IO[Either[AppError, List[OrderItem]]]

  def findByOrderId(orderId: OrderId): IO[Either[AppError, List[OrderItem]]]

  def createOrder(newOrderItem: NewOrderItem): IO[Either[AppError, OrderItem]]

  def findOrderById(orderItemId: Long): IO[Either[AppError, Option[OrderItem]]]

  def updateOrder(orderItem: OrderItem): IO[Either[AppError, OrderItem]]

  def deleteOrder(orderItemId: Long): IO[Either[AppError, SimpleMessage]]
}

object OrderItemService {
  private final class Impl(sql: OrderItemSql, transactor: Transactor[IO], orderService: OrderService) extends OrderItemService {
    override def createOrder(newOrderItem: NewOrderItem): IO[Either[AppError, OrderItem]] =
      sql
        .create(newOrderItem)
        .transact[IO](transactor)
        .map(_.left.map(InternalError(_)))

    override def findOrderById(orderItemId: Long): IO[Either[AppError, Option[OrderItem]]] =
      sql
        .findById(orderItemId)
        .transact[IO](transactor)
        .attempt
        .map(_.left.map(InternalError(_)))

    override def updateOrder(orderItem: OrderItem): IO[Either[AppError, OrderItem]] =
      sql
        .update(orderItem)
        .transact(transactor)
        .map(_.left.map(InternalError(_)))

    override def deleteOrder(orderItemId: Long): IO[Either[AppError, SimpleMessage]] =
      sql
        .delete(orderItemId)
        .transact(transactor)
        .map(_.left.map(InternalError(_)).map(deleted => SimpleMessage(s"Deleted $deleted rows")))

    override def findByOrderId(orderId: OrderId): IO[Either[AppError, List[OrderItem]]] =
      sql
        .findByOrderId(orderId)
        .transact(transactor)
        .map(_.left.map(InternalError(_)))

    //F[M[A, B]]  => B

    override def batchCreate(orderId: OrderId, newItems: List[NewOrderItem]): IO[Either[AppError, List[OrderItem]]] = ???
/*
      EitherT(orderService.findById(orderId))
        .flatMap { case Some(value) =>
          EitherT(newItems.map(_.focus(_.orderId)
            .replace(value.id))
            .map(createOrder)
            .sequence
            .map(_.sequence)
          )
        case None => EitherT.leftT(InternalError(new Exception()))
        }
        .value
    }
*/
  }

  def make(sql: OrderItemSql, transactor: Transactor[IO], orderService: OrderService): OrderItemService =
    new Impl(sql, transactor, orderService)

}
