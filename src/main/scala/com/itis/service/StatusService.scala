package com.itis.service

import cats.effect.IO
import com.itis.controller.endpoints.status
import com.itis.dao.StatusSql
import com.itis.domain.errors.{AppError, InternalError}
import com.itis.domain.{NewStatus, SimpleMessage, Status}
import doobie._
import doobie.implicits._

trait StatusService {
  def findAll(): IO[Either[AppError, List[Status]]]


  def create(newStatus: NewStatus): IO[Either[AppError, Status]]

  def findById(statusId: Long): IO[Either[AppError, Option[Status]]]

  def update(status: Status): IO[Either[AppError, Status]]

  def delete(statusId: Long): IO[Either[AppError, SimpleMessage]]
}

object StatusService {
  private final class Impl(sql: StatusSql, transactor: Transactor[IO]) extends StatusService {
    override def create(newStatus: NewStatus): IO[Either[AppError, Status]] =
      sql
        .create(newStatus)
        .transact(transactor)
        .map(_.left.map(InternalError(_)))

    override def findById(statusId: Long): IO[Either[AppError, Option[Status]]] =
      sql
        .findById(statusId)
        .transact(transactor)
        .attempt
        .map(_.left.map(InternalError(_)))

    override def update(status: Status): IO[Either[AppError, Status]] =
      sql
        .update(status)
        .transact(transactor)
        .map(_.left.map(InternalError(_)))

    override def delete(statusId: Long): IO[Either[AppError, SimpleMessage]] =
      sql
        .delete(statusId)
        .transact(transactor)
        .map(_.left.map(InternalError(_)).map(d => SimpleMessage(s"Deleted $d rows")))

    override def findAll(): IO[Either[AppError, List[Status]]] =
      sql
        .findAll()
        .transact(transactor)
        .map(_.left.map(InternalError(_)))
  }

  def make(sql: StatusSql, transactor: Transactor[IO]): StatusService = new Impl(sql, transactor)
}
