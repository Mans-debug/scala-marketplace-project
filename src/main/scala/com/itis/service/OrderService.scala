package com.itis.service

import cats.effect.IO
import com.itis.dao.OrderSql
import com.itis.domain.{NewOrder, Order, SimpleMessage}
import com.itis.domain.errors.{AppError, InternalError}
import com.itis.domain.types.{OrderId, UserId}
import doobie.Transactor
import doobie.implicits._

trait OrderService {
  def findBySellerId(userId: UserId): IO[Either[AppError, List[Order]]]

  def create(order: NewOrder): IO[Either[AppError, Order]]

  def findById(orderId: OrderId): IO[Either[AppError, Option[Order]]]

  def update(order: Order): IO[Either[AppError, Order]]

  def delete(orderId: OrderId): IO[Either[AppError, SimpleMessage]]

  def findByUserId(userId: UserId): IO[Either[AppError, List[Order]]]

}

object OrderService {
  private final class Impl(sql: OrderSql, transactor: Transactor[IO]) extends OrderService {
    override def create(order: NewOrder): IO[Either[AppError, Order]] =
      sql
        .create(order)
        .transact(transactor)
        .map(_.left.map(InternalError(_)))

    override def findById(orderId: OrderId): IO[Either[AppError, Option[Order]]] =
      sql
        .findById(orderId)
        .transact(transactor)
        .attempt
        .map(_.left.map(InternalError(_)))

    override def update(order: Order): IO[Either[AppError, Order]] =
      sql
        .update(order)
        .transact(transactor)
        .map(_.left.map(InternalError(_)))

    override def delete(orderId: OrderId): IO[Either[AppError, SimpleMessage]] =
      sql
        .delete(orderId)
        .transact(transactor)
        .map(_.left.map(InternalError(_)).map(deleted => SimpleMessage(s"Deleted $deleted rows")))

    override def findBySellerId(userId: UserId): IO[Either[AppError, List[Order]]] =
      sql
        .findBySellerId(userId)
        .transact(transactor)
        .map(_.left.map(InternalError(_)))

    override def findByUserId(userId: UserId): IO[Either[AppError, List[Order]]] =
      sql
        .findByUserId(userId)
        .transact(transactor)
        .map(_.left.map(InternalError(_)))
  }

  def make(sql: OrderSql, transactor: Transactor[IO]): OrderService = new Impl(sql, transactor)
}
