package com.itis.service

import cats.implicits.catsSyntaxEitherId
import com.itis.config.JwtConfig
import com.itis.domain.errors.{JwtDecodingError, JwtError}
import com.itis.domain.types.JwtToken
import com.itis.domain.{User, UserPrincipal}
import io.circe.generic.auto._
import io.circe.parser
import io.circe.syntax.EncoderOps
import pdi.jwt.{JwtAlgorithm, JwtCirce, JwtClaim}

import java.time.Instant
import scala.util.{Failure, Success}

trait TokenService {
  def encode(user: User): JwtToken

  def decode(jwtToken: JwtToken): Either[JwtError, UserPrincipal]
}

object TokenService {
  private final class Impl(jwtConf: JwtConfig) extends TokenService {
    private val algo = JwtAlgorithm.HS256

    override def encode(user: User): JwtToken = {
      val claim = JwtClaim(
        content = UserPrincipal(user.id, user.role).asJson.toString(),
        expiration = Some(Instant.now.plusSeconds(jwtConf.expirationSec).getEpochSecond),
        issuedAt = Some(Instant.now().getEpochSecond)
      )
      val token = JwtCirce.encode(claim, jwtConf.secret, algo)
      JwtToken(s"Bearer $token")
    }

    override def decode(jwtToken: JwtToken): Either[JwtError, UserPrincipal] = {
      def rmPrefixIfPresent(jwtToken: JwtToken): JwtToken = JwtToken(jwtToken.value.replace("Bearer ", ""))

      JwtCirce.decode(rmPrefixIfPresent(jwtToken).value, jwtConf.secret, Seq(algo)) match {
        case Success(jwtClaim) =>
          parser
            .decode[UserPrincipal](jwtClaim.content)
            .left
            .map(decodingEx => JwtDecodingError(s"Could not parse content of jwt token ${decodingEx.getMessage}"))
        case Failure(exception) => JwtDecodingError(s"Invalid token\n${exception.getMessage}").asLeft[UserPrincipal]
      }

    }
  }

  def make(jwtConfig: JwtConfig): TokenService = new Impl(jwtConfig)
}
