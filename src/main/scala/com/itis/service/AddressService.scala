package com.itis.service

import cats.effect.IO
import com.itis.dao.AddressSql
import com.itis.domain.errors.AppError
import com.itis.domain.{Address, NewAddress, SimpleMessage}
import com.itis.domain.errors.InternalError
import doobie.Transactor
import doobie.implicits._
import doobie.util.log
import tofu.logging.Logging

import java.sql.SQLException
import scala.tools.nsc.interpreter.Naming.sessionNames.res

trait AddressService {
  def createAddress(address: NewAddress): IO[Either[AppError, Address]]

  def findAddressById(addressId: Long): IO[Either[AppError, Option[Address]]]

  def updateAddress(address: Address): IO[Either[AppError, Address]]

  def deleteAddress(addressId: Long): IO[Either[AppError, SimpleMessage]]
}

object AddressService {
  private final class Impl(sql: AddressSql, transactor: Transactor[IO]) extends AddressService {
    override def createAddress(address: NewAddress): IO[Either[AppError, Address]] =
      sql
        .createAddress(address)
        .transact[IO](transactor)
        .map(_.left.map(InternalError(_)))

    override def findAddressById(addressId: Long): IO[Either[AppError, Option[Address]]] =
      sql
        .findById(addressId)
        .transact[IO](transactor)
        .attemptSql
        .map(_.left.map(InternalError(_)))

    override def updateAddress(address: Address): IO[Either[AppError, Address]] =
      sql
        .update(address)
        .transact[IO](transactor)
        .map(_.left.map(InternalError(_)))

    override def deleteAddress(addressId: Long): IO[Either[AppError, SimpleMessage]] =
      sql
        .deleteAddress(addressId)
        .transact[IO](transactor)
        .map(_.left.map(InternalError(_)).map(deleted => SimpleMessage(s"Deleted $deleted rows")))
  }

  private final class LoggingImpl(impl: Impl)(implicit log: Logging[IO]) extends AddressService {
    override def createAddress(address: NewAddress): IO[Either[AppError, Address]] =
      for {
        _ <- log.info(s"Creating address ${address.fullAddress}")
        res <- impl.createAddress(address)
        _ <- res match {
          case Left(ex)       => log.error(s"Could not create address ${ex.exString}")
          case Right(address) => log.info(s"Created address with id ${address.id}")
        }
      } yield res

    override def findAddressById(addressId: Long): IO[Either[AppError, Option[Address]]] =
      for {
        _ <- log.info(s"Fetching address by id $addressId")
        res <- impl.findAddressById(addressId)
        _ <- res match {
          case Left(ex)     => log.error(s"Could not fetch address ${ex.exString}")
          case Right(value) => log.info("Successfully fetched address")
        }
      } yield res

    override def updateAddress(address: Address): IO[Either[AppError, Address]] =
      for {
        _ <- log.info(s"Updating address with id ${address.id}")
        res <- impl.updateAddress(address)
        _ <- res match {
          case Left(ex)     => log.error(s"Could not update address ${ex.exString}")
          case Right(value) => log.error(s"Successfully update address with id ${value.id}")
        }
      } yield res

    override def deleteAddress(addressId: Long): IO[Either[AppError, SimpleMessage]] =
      for {
        _ <- log.info(s"Deleting address by id $addressId")
        res <- impl.deleteAddress(addressId)
        _ <- res match {
          case Left(ex)     => log.error(s"Could not delete address with id $addressId ${ex.exString}")
          case Right(value) => log.info(value.message)
        }
      } yield res
  }

  def make(sql: AddressSql, transactor: Transactor[IO])(implicit log: Logging[IO]): AddressService = new LoggingImpl(new Impl(sql, transactor))
}
