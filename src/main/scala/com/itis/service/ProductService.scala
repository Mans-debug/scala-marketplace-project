package com.itis.service

import cats.effect.IO
import cats.instances.uuid
import com.itis.dao.ProductSql
import com.itis.domain.{NewProduct, Product, ProductFilter, SimpleMessage, errors, types}
import com.itis.domain.errors.AppError
import com.itis.domain.types.{CategoryId, ProductId, UserId}
import doobie.util.transactor.Transactor
import tofu.logging.Logging
import doobie._
import doobie.implicits._
import doobie.util.transactor
import org.checkerframework.checker.units.qual.s

trait ProductService {
  def filterProducts(filter: ProductFilter): IO[Either[AppError, List[Product]]]

  def findAll(): IO[Either[AppError, List[Product]]]

  def findBySellerId(userId: UserId): IO[Either[AppError, List[Product]]]

  def findByCategoryId(categoryId: CategoryId): IO[Either[AppError, List[Product]]]

  def createProduct(newProduct: NewProduct): IO[Either[AppError, Product]]

  def findProductById(productId: ProductId): IO[Either[AppError, Option[Product]]]

  def updateProduct(product: Product): IO[Either[AppError, Product]]

  def deleteProduct(productId: ProductId): IO[Either[AppError, SimpleMessage]]
}

object ProductService {
  private final class Impl(sql: ProductSql, transactor: Transactor[IO]) extends ProductService {
    override def createProduct(newProduct: NewProduct): IO[Either[AppError, Product]] =
      sql
        .createProduct(newProduct)
        .transact[IO](transactor)
        .map(_.left.map(errors.InternalError(_)))

    override def findProductById(productId: ProductId): IO[Either[AppError, Option[Product]]] =
      sql
        .findProductById(productId)
        .transact[IO](transactor)
        .attemptSql
        .map(_.left.map(errors.InternalError(_)))

    override def updateProduct(product: Product): IO[Either[AppError, Product]] =
      sql
        .updateProduct(product)
        .transact[IO](transactor)
        .map(_.left.map(errors.InternalError(_)))

    override def deleteProduct(productId: ProductId): IO[Either[AppError, SimpleMessage]] =
      sql
        .deleteProduct(productId)
        .transact[IO](transactor)
        .map(_.left.map(errors.InternalError(_)).map(d => SimpleMessage(s"Deleted $d rows")))

    override def findByCategoryId(categoryId: CategoryId): IO[Either[AppError, List[Product]]] =
      sql
        .findByCategoryId(categoryId)
        .transact(transactor)
        .map(_.left.map(errors.InternalError(_)))

    override def findBySellerId(userId: UserId): IO[Either[AppError, List[Product]]] =
      sql
        .findBySellerId(userId)
        .transact(transactor)
        .map(_.left.map(errors.InternalError(_)))

    override def findAll(): IO[Either[AppError, List[Product]]] =
      sql
        .findAll()
        .transact(transactor)
        .map(_.left.map(errors.InternalError(_)))

    override def filterProducts(filter: ProductFilter): IO[Either[AppError, List[Product]]] =
      sql
        .filterProducts(filter)
        .transact(transactor)
        .map(_.left.map(errors.InternalError(_)))
  }

  private final class LoggingImpl(impl: Impl)(implicit log: Logging[IO]) extends ProductService {
    override def createProduct(newProduct: NewProduct): IO[Either[AppError, Product]] =
      for {
        _ <- log.info(s"Creating product with name ${newProduct.title}")
        res: Either[AppError, Product] <- impl.createProduct(newProduct)
        _ <- res match {
          case Right(prod) => log.info(s"Successfully created product with id ${prod.id}")
          case Left(error) => log.error(s"Could not create product. Ex message ${error.message} ${error.exString}")
        }
      } yield res

    override def findProductById(productId: ProductId): IO[Either[AppError, Option[Product]]] =
      for {
        _ <- log.info(s"Trying to find product by id ${productId.value}")
        res <- impl.findProductById(productId)
        _ <- res match {
          case Right(optProduct) => log.info(s"Fetched product with id ${optProduct.map(_.id)}")
          case Left(error)       => log.error(s"Exception when fetching product by id $uuid, ${error.toString}")
        }
      } yield res

    override def updateProduct(product: Product): IO[Either[AppError, Product]] =
      for {
        _ <- log.info(s"Updating product with id ${product.id}")
        res: Either[AppError, Product] <- impl.updateProduct(product)
        _ <- res match {
          case Right(product) => log.info("Successfully updated product")
          case Left(appError) => log.error(s"Could not update product ${appError.message} ${appError.exString}")
        }
      } yield res

    override def deleteProduct(productId: ProductId): IO[Either[AppError, SimpleMessage]] =
      for {
        _ <- log.info(s"Deleting product with id ${productId.value}")
        res <- impl.deleteProduct(productId)
        _ <- res match {
          case Right(deleted) => log.info(deleted.message)
          case Left(error)    => log.error(s"Could not delete product ${error.message} ${error.exString}")
        }
      } yield res

    override def findByCategoryId(categoryId: CategoryId): IO[Either[AppError, List[Product]]] =
      for {
        _ <- log.info(s"Fetching all products by categoryId ${categoryId.value}")
        res <- impl.findByCategoryId(categoryId)
      } yield res

    override def findBySellerId(userId: UserId): IO[Either[AppError, List[Product]]] =
      for {
        _ <- log.info(s"Searching seller's goods by userId ${userId.value}")
        res <- impl.findBySellerId(userId)
      } yield res

    override def findAll(): IO[Either[AppError, List[Product]]] = impl.findAll()

    override def filterProducts(filter: ProductFilter): IO[Either[AppError, List[Product]]] = impl.filterProducts(filter)
  }

  def make(sql: ProductSql, transactor: Transactor[IO])(implicit log: Logging[IO]): ProductService =
    new LoggingImpl(new Impl(sql, transactor))
}
