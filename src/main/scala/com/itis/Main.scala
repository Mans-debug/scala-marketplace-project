package com.itis

import cats.effect.kernel.Resource
import cats.effect.{ExitCode, IO, IOApp}
import com.comcast.ip4s.{IpLiteralSyntax, Port}
import com.itis.config.{AppConfig, DbConfig}
import com.itis.controller._
import com.itis.dao._
import com.itis.service._
import doobie.util.transactor.Transactor
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.implicits._
import org.http4s.server.Router
import sttp.tapir.server.http4s.Http4sServerInterpreter
import sttp.tapir.swagger.bundle.SwaggerInterpreter
import tofu.logging.Logging

object Main extends IOApp {

  implicit val logging: Logging[IO] = Logging.Make.plain[IO].byName("Main")

  private def initDb(config: DbConfig): Transactor[IO] =
    Transactor.fromDriverManager[IO](
      config.driver,
      config.url,
      config.user,
      config.password
    )

  override def run(args: List[String]): IO[ExitCode] =
    (for {
      config <- Resource.eval(AppConfig.load)
      transactor = initDb(config.dbConfig)

      userSql = UserSql.make
      addressSql = AddressSql.make
      basketSql = BasketSql.make
      categorySql = CategorySql.make
      orderItemSql = OrderItemSql.make
      orderSql = OrderSql.make
      productBasketSql = ProductBasketSql.make
      productFileSql = ProductBasketSql.make
      productSql = ProductSql.make
      statusSql = StatusSql.make

      tokenService = TokenService.make(config.jwtConfig)
      implicit0(it: TokenService) <- Resource.pure(tokenService)
      authService = AuthService.make(userSql, transactor, tokenService)
      userService = UserService.make(userSql, transactor)
      addressService = AddressService.make(addressSql, transactor)
      basketService = BasketService.make(basketSql, transactor)
      categoryService = CategoryService.make(categorySql, transactor)
      orderService = OrderService.make(orderSql, transactor)
      orderItemService = OrderItemService.make(orderItemSql, transactor, orderService)
      productBasketService = ProductBasketService.make(productBasketSql, transactor, basketService)
      productService = ProductService.make(productSql, transactor)
      statusService = StatusService.make(statusSql, transactor)

      userController = UserController.make(userService, authService, tokenService, orderService, productService)
      addressController = AddressController.make(addressService, tokenService)
      basketController = BasketController.make(basketService, productBasketService)
      categoryController = CategoryController.make(categoryService, productService)
      orderController = OrderController.make(orderService, orderItemService)
      orderItemController = OrderItemController.make(orderItemService)
      productBasketController = ProductBasketController.make(productBasketService)
      productController = ProductController.make(productService)
      statusController = StatusController.make(statusService)

      endpoints = List(
        userController.all,
        addressController.all,
        basketController.all,
        categoryController.all,
        orderController.all,
        orderItemController.all,
        productBasketController.all,
        productController.all,
        statusController.all
      ).flatten
      swaggerEp = SwaggerInterpreter().fromEndpoints[IO](endpoints.map(_.endpoint), "MarketplaceApp", "0.0.1")
      routes = Http4sServerInterpreter[IO]().toRoutes(
        endpoints ++
          swaggerEp)
      httpApp = Router("/" -> routes).orNotFound
      _ <- EmberServerBuilder
        .default[IO]
        .withHost(ipv4"0.0.0.0")
        .withPort(Port.fromInt(config.serverConfig.port).getOrElse(port"8082"))
        .withHttpApp(httpApp)
        .build
    } yield ()).useForever.as(ExitCode.Success)
}
