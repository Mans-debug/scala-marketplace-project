package com.itis.controller

import cats.effect.IO
import com.itis.controller.EndpointUtils.authenticate
import com.itis.service.{AddressService, AuthService, BasketService, ProductBasketService, ProductService, TokenService}
import sttp.tapir.server.ServerEndpoint

trait BasketController {

  def createBasket: ServerEndpoint[Any, IO]

  def findBasketById: ServerEndpoint[Any, IO]

  def updateBasket: ServerEndpoint[Any, IO]

  def deleteBasket: ServerEndpoint[Any, IO]

  def findProductsByBasketId: ServerEndpoint[Any, IO]

  def all: List[ServerEndpoint[Any, IO]]
}

object BasketController {
  private final class Impl(basketService: BasketService, productBasketService: ProductBasketService)
                          (implicit tokenService: TokenService) extends BasketController {

    override def createBasket: ServerEndpoint[Any, IO] =
      authenticate(endpoints.basket.create)
        .serverLogic{principal => newBasket => basketService.createBasket(newBasket)}

    override def findBasketById: ServerEndpoint[Any, IO] =
      authenticate(endpoints.basket.findById)
        .serverLogic{principal => basketId => basketService.findById(basketId)}

    override def updateBasket: ServerEndpoint[Any, IO] =
      authenticate(endpoints.basket.update)
        .serverLogic{principal => basket => basketService.update(basket)}

    override def deleteBasket: ServerEndpoint[Any, IO] =
      authenticate(endpoints.basket.delete)
        .serverLogic{principal => basketId => basketService.deleteBasket(basketId)}

    override def findProductsByBasketId: ServerEndpoint[Any, IO] =
      authenticate(endpoints.basket.products)
        .serverLogic { principal => basketId => productBasketService.findByBasketId(basketId)}

    override def all: List[ServerEndpoint[Any, IO]] = List(
      createBasket,
      findBasketById,
      updateBasket,
      deleteBasket,
      findProductsByBasketId
    )

  }

  def make(basketService: BasketService, productBasketService: ProductBasketService)
          (implicit tokenService: TokenService): BasketController = new Impl(basketService, productBasketService)
}




