package com.itis.controller

import cats.effect.IO
import com.itis.controller.EndpointUtils.authenticate
import com.itis.service.{OrderItemService, OrderService, ProductBasketService, TokenService}
import sttp.tapir.server.ServerEndpoint

trait ProductBasketController {

  def createProductBasket: ServerEndpoint[Any, IO]

  def findProductBasketById: ServerEndpoint[Any, IO]

  def updateProductBasket: ServerEndpoint[Any, IO]

  def deleteProductBasket: ServerEndpoint[Any, IO]

  def all: List[ServerEndpoint[Any, IO]]
}

object ProductBasketController {
  private final class Impl(productBasketService: ProductBasketService)
                          (implicit tokenService: TokenService) extends ProductBasketController {

    override def createProductBasket: ServerEndpoint[Any, IO] =
      authenticate(endpoints.basketitem.create)
        .serverLogic(principal =>
        newProductBasket => productBasketService.create(newProductBasket))

    override def findProductBasketById: ServerEndpoint[Any, IO] =
      authenticate(endpoints.basketitem.findById)
        .serverLogic(principal =>
        productBasketId => productBasketService.findById(productBasketId))

    override def updateProductBasket: ServerEndpoint[Any, IO] =
      authenticate(endpoints.basketitem.update)
        .serverLogic(principal =>
        productBasket => productBasketService.update(productBasket))

    override def deleteProductBasket: ServerEndpoint[Any, IO] =
      authenticate(endpoints.basketitem.delete)
        .serverLogic(principal =>
        productBasketId => productBasketService.delete(productBasketId))

    override def all: List[ServerEndpoint[Any, IO]] = List(
      createProductBasket,
      findProductBasketById,
      updateProductBasket,
      deleteProductBasket
    )
  }
  def make(productBasketService: ProductBasketService)
          (implicit tokenService: TokenService): ProductBasketController = new Impl(productBasketService)
}




