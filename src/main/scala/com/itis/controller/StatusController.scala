package com.itis.controller

import cats.effect.IO
import com.itis.controller.EndpointUtils.authenticate
import com.itis.service.{ProductService, StatusService, TokenService}
import sttp.tapir.server.ServerEndpoint

trait StatusController {

  def createStatus: ServerEndpoint[Any, IO]

  def findStatusById: ServerEndpoint[Any, IO]

  def updateStatus: ServerEndpoint[Any, IO]

  def deleteStatus: ServerEndpoint[Any, IO]

  def findAll: ServerEndpoint[Any, IO]

  def all: List[ServerEndpoint[Any, IO]]
}

object StatusController {
  private final class Impl(statusService: StatusService)
                          (implicit tokenService: TokenService) extends StatusController {

    override def createStatus: ServerEndpoint[Any, IO] =
      authenticate(endpoints.status.create)
        .serverLogic(principal =>
        newStatus => statusService.create(newStatus))

    override def findStatusById: ServerEndpoint[Any, IO] =
      authenticate(endpoints.status.findById)
        .serverLogic(principal =>
        statusId => statusService.findById(statusId))

    override def updateStatus: ServerEndpoint[Any, IO] =
      authenticate(endpoints.status.update)
        .serverLogic(principal =>
        status => statusService.update(status))

    override def deleteStatus: ServerEndpoint[Any, IO] =
      authenticate(endpoints.status.delete)
        .serverLogic(principal =>
        statusId => statusService.delete(statusId))

    override def findAll: ServerEndpoint[Any, IO] =
      authenticate(endpoints.status.all)
        .serverLogic(principal =>
        _ => statusService.findAll())

    override def all: List[ServerEndpoint[Any, IO]] = List(
      createStatus,
      findStatusById,
      updateStatus,
      deleteStatus,
      findAll
    )

  }

  def make(statusService: StatusService)
          (implicit tokenService: TokenService): StatusController = new Impl(statusService)
}




