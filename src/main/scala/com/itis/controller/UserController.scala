package com.itis.controller

import cats.effect.IO
import com.itis.controller.EndpointUtils.authenticate
import com.itis.domain.SimpleMessage
import com.itis.domain.errors.JwtError._
import com.itis.service.{AuthService, OrderService, ProductService, TokenService, UserService}
import sttp.tapir.server.ServerEndpoint

trait UserController {
  def signIn: ServerEndpoint[Any, IO]

  def signUp: ServerEndpoint[Any, IO]

  def updateUser: ServerEndpoint[Any, IO]

  def findUserById: ServerEndpoint[Any, IO]

  def deleteUser: ServerEndpoint[Any, IO]

  def findOrdersByUserId: ServerEndpoint[Any, IO]

  def findSellerProducts: ServerEndpoint[Any, IO]

  def findSellerOrders: ServerEndpoint[Any, IO]

  def all: List[ServerEndpoint[Any, IO]]
}

object UserController {
  final private class Impl(userService: UserService,
                           authService: AuthService,
                           tokenService: TokenService,
                           orderService: OrderService,
                           productService: ProductService)
    extends UserController {
    private implicit val authTokenService: TokenService = tokenService

    override def signUp: ServerEndpoint[Any, IO] =
      endpoints.user.signUp.serverLogic(createUser => userService.createUser(createUser))

    override def signIn: ServerEndpoint[Any, IO] =
      endpoints.user.signIn.
        serverLogic(signInForm => authService.auth(signInForm))

    override def updateUser: ServerEndpoint[Any, IO] =
      authenticate(endpoints.user.updateUser)
        .serverLogic { principal => user => userService.updateUser(user) }

    override def findUserById: ServerEndpoint[Any, IO] =
      authenticate(endpoints.user.getUser)
        .serverLogic(principal => {
          //todo check role
          uuid => userService.userById(uuid)
        })

    override def deleteUser: ServerEndpoint[Any, IO] = {
      authenticate(endpoints.user.deleteUser)
        .serverLogic { principal => uuid =>
          userService
            .deleteUserById(uuid)
            .map(_.map(deleted => SimpleMessage(s"Deleted rows $deleted")))
        }
    }

    override def findOrdersByUserId: ServerEndpoint[Any, IO] =
      authenticate(endpoints.user.orders)
        .serverLogic(principal =>
        userId => orderService.findByUserId(userId))

    override def findSellerProducts: ServerEndpoint[Any, IO] =
      authenticate(endpoints.user.seller.products)
        .serverLogic(principal =>
        userId => productService.findBySellerId(userId))

    override def findSellerOrders: ServerEndpoint[Any, IO] =
      authenticate(endpoints.user.seller.orders)
        .serverLogic(principal =>
        userId => orderService.findBySellerId(userId))

    override def all: List[ServerEndpoint[Any, IO]] = List(
      signIn,
      signUp,
      updateUser,
      findUserById,
      deleteUser,
      findOrdersByUserId,
      findSellerProducts,
      findSellerOrders
    )
  }

  def make(userService: UserService,
           authService: AuthService,
           tokenService: TokenService,
           orderService: OrderService,
           productService: ProductService): UserController =
    new Impl(userService, authService, tokenService, orderService, productService)

}
