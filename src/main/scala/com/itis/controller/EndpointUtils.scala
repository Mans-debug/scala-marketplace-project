package com.itis.controller

import cats.effect.IO
import com.itis.domain.UserPrincipal
import com.itis.domain.errors.JwtError
import com.itis.domain.errors.JwtError._
import com.itis.domain.types.JwtToken
import com.itis.service.{Converter, TokenService}
import sttp.tapir.Endpoint
import sttp.tapir.server.PartialServerEndpoint

object EndpointUtils {

  def authenticate[I, E, O](endpoint: Endpoint[JwtToken, I, E, O, Any])(implicit
    exConverter: Converter[JwtError, E],
    tokenService: TokenService
  ): PartialServerEndpoint[JwtToken, UserPrincipal, I, E, O, Any, IO] =
    endpoint.serverSecurityLogicPure[UserPrincipal, IO] { jwt =>
      tokenService.decode(jwt).left.map(exConverter.convert)
    }
}
