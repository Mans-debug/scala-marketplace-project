package com.itis.controller

import cats.effect.IO
import com.itis.controller.EndpointUtils.authenticate
import com.itis.service.{OrderItemService, TokenService}
import sttp.tapir.server.ServerEndpoint

trait OrderItemController {

  def createOrderItem: ServerEndpoint[Any, IO]

  def findOrderItemById: ServerEndpoint[Any, IO]

  def updateOrderItem: ServerEndpoint[Any, IO]

  def deleteOrderItem: ServerEndpoint[Any, IO]

  def batchCreate: ServerEndpoint[Any, IO]

  def all: List[ServerEndpoint[Any, IO]]
}

object OrderItemController {
  private final class Impl(orderItemService: OrderItemService)
                          (implicit tokenService: TokenService) extends OrderItemController {

    override def createOrderItem: ServerEndpoint[Any, IO] =
      authenticate(endpoints.orderitem.create)
        .serverLogic(principal => newOrderItem => orderItemService.createOrder(newOrderItem))

    override def findOrderItemById: ServerEndpoint[Any, IO] =
      authenticate(endpoints.orderitem.findById)
        .serverLogic(principal => orderItemId => orderItemService.findOrderById(orderItemId))

    override def updateOrderItem: ServerEndpoint[Any, IO] =
      authenticate(endpoints.orderitem.update)
        .serverLogic(principal => orderItem => orderItemService.updateOrder(orderItem))

    override def deleteOrderItem: ServerEndpoint[Any, IO] =
      authenticate(endpoints.orderitem.delete)
        .serverLogic(principal => orderItemId => orderItemService.deleteOrder(orderItemId))


    override def batchCreate: ServerEndpoint[Any, IO] =
      authenticate(endpoints.orderitem.batchCreate)
        .serverLogic(principal =>
        {case (orderId, newItems) => orderItemService.batchCreate(orderId, newItems)})

    override def all: List[ServerEndpoint[Any, IO]] = List(
      createOrderItem,
      findOrderItemById,
      updateOrderItem,
      deleteOrderItem,
      batchCreate
    )

  }
  def make(orderItemService: OrderItemService)
          (implicit tokenService: TokenService): OrderItemController = new Impl(orderItemService)
}




