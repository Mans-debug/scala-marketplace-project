package com.itis.controller

import cats.effect.IO
import com.itis.controller.EndpointUtils.authenticate
import com.itis.domain.{Address, NewAddress}
import com.itis.domain.errors.AppError
import com.itis.service.{AddressService, AuthService, TokenService}
import sttp.tapir.server.ServerEndpoint

trait AddressController {

  def createAddress: ServerEndpoint[Any, IO]

  def findAddressById: ServerEndpoint[Any, IO]

  def updateAddress: ServerEndpoint[Any, IO]

  def deleteAddress: ServerEndpoint[Any, IO]

  def all: List[ServerEndpoint[Any, IO]]

}

object AddressController {
  private final class Impl(addressService: AddressService, tokenService: TokenService) extends AddressController {
    private implicit val authTokenService: TokenService = tokenService
    override def createAddress: ServerEndpoint[Any, IO] =
      authenticate(endpoints.address.create)
        .serverLogic{principal => newAddress => addressService.createAddress(newAddress)}


    override def findAddressById: ServerEndpoint[Any, IO] =
      authenticate(endpoints.address.findById)
        .serverLogic{principal => addressId => addressService.findAddressById(addressId)}

    override def updateAddress: ServerEndpoint[Any, IO] =
      authenticate(endpoints.address.update)
        .serverLogic{principal => address => addressService.updateAddress(address)}

    override def deleteAddress: ServerEndpoint[Any, IO] =
      authenticate(endpoints.address.delete)
        .serverLogic{principal => addressId => addressService.deleteAddress(addressId)}

    override def all: List[ServerEndpoint[Any, IO]] = List(
      createAddress,
      findAddressById,
      updateAddress,
      deleteAddress
    )
  }
  def make(addressService: AddressService, tokenService: TokenService): AddressController = new Impl(addressService, tokenService)
}

