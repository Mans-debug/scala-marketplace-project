package com.itis.controller

import com.itis.domain._
import com.itis.domain.errors.{AppError, InternalError}
import com.itis.domain.types._
import io.circe.generic.auto._
import sttp.model.headers.WWWAuthenticateChallenge
import sttp.tapir._
import sttp.tapir.codec.newtype.codecForNewType
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.json.circe.jsonBody

object endpoints {

  object user {

    val getUser: Endpoint[JwtToken, UserId, InternalError, Option[User], Any] =
      endpoint.get
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("users" / path[UserId]("userId"))
        .tag("User")
        .errorOut(jsonBody[InternalError])
        .out(jsonBody[Option[User]])

    val signIn: PublicEndpoint[SignInForm, AppError, UserPrincipalWithToken, Any] =
      endpoint.post
        .in("signIn")
        .tag("User")
        .in(jsonBody[SignInForm])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[UserPrincipalWithToken])

    val signUp: PublicEndpoint[SignUpForm, AppError, User, Any] =
      endpoint.post
        .in("signUp")
        .tag("User")
        .in(jsonBody[SignUpForm])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[User])

    val updateUser: Endpoint[JwtToken, User, AppError, User, Any] =
      endpoint
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .put
        .in("users")
        .tag("User")
        .in(jsonBody[User])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[User])

    val deleteUser: Endpoint[JwtToken, UserId, AppError, SimpleMessage, Any] =
      endpoint
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .delete
        .in("users" / path[UserId]("userId"))
        .tag("User")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[SimpleMessage])

    val orders: Endpoint[JwtToken, UserId, AppError, List[Order], Any] =
      endpoint.get
        .description("Get user's orders")
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("users" / path[UserId]("userId") / "orders")
        .withTags(List("User", "Order"))
        .errorOut(jsonBody[AppError])
        .out(jsonBody[List[Order]])

    object seller {
      val products: Endpoint[JwtToken, UserId, AppError, List[Product], Any] =
        endpoint.get
          .description("Find seller's goods")
          .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
          .mapSecurityIn(header => JwtToken(header))(_.value)
          .in("sellers" / path[UserId]("userId") / "products")
          .withTags(List("User", "Product"))
          .errorOut(jsonBody[AppError])
          .out(jsonBody[List[Product]])

      val orders =
        endpoint.get
          .description("Find seller's orders")
          .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
          .mapSecurityIn(header => JwtToken(header))(_.value)
          .in("sellers" / path[UserId]("userId") / "orders")
          .withTags(List("User", "Order"))
          .errorOut(jsonBody[AppError])
          .out(jsonBody[List[Order]])

    }

  }

  object address {
    val findById: Endpoint[JwtToken, Long, AppError, Option[Address], Any] =
      endpoint.get
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("adresses" / path[Long]("addressId"))
        .tag("Address")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Option[Address]])

    val create: Endpoint[JwtToken, NewAddress, AppError, Address, Any] =
      endpoint.post
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("adresses")
        .tag("Address")
        .in(jsonBody[NewAddress])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Address])

    val update: Endpoint[JwtToken, Address, AppError, Address, Any] =
      endpoint.put
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("adresses")
        .tag("Address")
        .in(jsonBody[Address])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Address])

    val delete: Endpoint[JwtToken, Long, AppError, SimpleMessage, Any] =
      endpoint.delete
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("adresses" / path[Long]("addressId"))
        .tag("Address")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[SimpleMessage])

  }

  object category {
    val findById: Endpoint[JwtToken, CategoryId, AppError, Option[Category], Any] =
      endpoint.get
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("categories" / path[CategoryId]("categoryId"))
        .tag("Category")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Option[Category]])

    val create: Endpoint[JwtToken, NewCategory, AppError, Category, Any] =
      endpoint.post
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("categories")
        .tag("Category")
        .in(jsonBody[NewCategory])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Category])

    val update: Endpoint[JwtToken, Category, AppError, Category, Any] =
      endpoint.put
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("categories")
        .tag("Category")
        .in(jsonBody[Category])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Category])

    val delete: Endpoint[JwtToken, CategoryId, AppError, SimpleMessage, Any] =
      endpoint.delete
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("categories" / path[CategoryId]("categoryId"))
        .tag("Category")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[SimpleMessage])


    val all: Endpoint[JwtToken, Unit, AppError, List[Category], Any] =
      endpoint.get
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("categories")
        .tag("Category")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[List[Category]])


    val products: Endpoint[JwtToken, CategoryId, AppError, List[Product], Any] =
      endpoint.get
        .description("Find products by specified category id")
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("categories" / path[CategoryId]("categoryId") / "products")
        .withTags(List("Category", "Product"))
        .errorOut(jsonBody[AppError])
        .out(jsonBody[List[Product]])
  }

  object product {
    val findAll: Endpoint[JwtToken, Unit, AppError, List[Product], Any] =
      endpoint.get
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("products")
        .tag("Product")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[List[Product]])

    val findById: Endpoint[JwtToken, ProductId, AppError, Option[Product], Any] =
      endpoint.get
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("products" / path[ProductId]("productId"))
        .tag("Product")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Option[Product]])

    val create: Endpoint[JwtToken, NewProduct, AppError, Product, Any] =
      endpoint.post
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("products")
        .tag("Product")
        .in(jsonBody[NewProduct])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Product])

    val update: Endpoint[JwtToken, Product, AppError, Product, Any] =
      endpoint.put
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("products")
        .tag("Product")
        .in(jsonBody[Product])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Product])

    val delete: Endpoint[JwtToken, ProductId, AppError, SimpleMessage, Any] =
      endpoint.delete
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("products" / path[ProductId]("productId"))
        .tag("Product")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[SimpleMessage])

    val filter =
      endpoint.post
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("products" / "filter")
        .tag("Product")
        .in(jsonBody[ProductFilter])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[List[Product]])

  }

  object status {
    val findById: Endpoint[JwtToken, Long, AppError, Option[Status], Any] =
      endpoint.get
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("statuses" / path[Long]("statusId"))
        .tag("Status")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Option[Status]])

    val create: Endpoint[JwtToken, NewStatus, AppError, Status, Any] =
      endpoint.post
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("statuses")
        .tag("Status")
        .in(jsonBody[NewStatus])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Status])

    val update: Endpoint[JwtToken, Status, AppError, Status, Any] =
      endpoint.put
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("statuses")
        .tag("Status")
        .in(jsonBody[Status])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Status])

    val delete: Endpoint[JwtToken, Long, AppError, SimpleMessage, Any] =
      endpoint.delete
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("statuses" / path[Long]("statusId"))
        .tag("Status")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[SimpleMessage])

    val all: Endpoint[JwtToken, Unit, AppError, List[Status], Any] =
      endpoint.get
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("statuses")
        .tag("Status")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[List[Status]])
  }

  object order {
    val findById: Endpoint[JwtToken, OrderId, AppError, Option[Order], Any] =
      endpoint.get
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("orders" / path[OrderId]("orderId"))
        .tag("Order")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Option[Order]])

    val create: Endpoint[JwtToken, NewOrder, AppError, Order, Any] =
      endpoint.post
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("orders")
        .tag("Order")
        .in(jsonBody[NewOrder])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Order])

    val update: Endpoint[JwtToken, Order, AppError, Order, Any] =
      endpoint.put
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("orders")
        .tag("Order")
        .in(jsonBody[Order])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Order])

    val delete: Endpoint[JwtToken, OrderId, AppError, SimpleMessage, Any] =
      endpoint.delete
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("orders" / path[OrderId]("orderId"))
        .tag("Order")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[SimpleMessage])

    val items: Endpoint[JwtToken, OrderId, AppError, List[OrderItem], Any] =
      endpoint.get
        .description("Find items from order")
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("orders" / path[OrderId]("orderId") / "products")
        .withTags(List("Order", "Order items"))
        .errorOut(jsonBody[AppError])
        .out(jsonBody[List[OrderItem]])

  }

  object orderitem {
    val findById: Endpoint[JwtToken, Long, AppError, Option[OrderItem], Any] =
      endpoint.get
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("orders" / "items" / path[Long]("orderItemId"))
        .tag("Order items")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Option[OrderItem]])

    val create: Endpoint[JwtToken, NewOrderItem, AppError, OrderItem, Any] =
      endpoint.post
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("orders" / "items")
        .tag("Order items")
        .in(jsonBody[NewOrderItem])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[OrderItem])

    val update: Endpoint[JwtToken, OrderItem, AppError, OrderItem, Any] =
      endpoint.put
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("orders" / "items")
        .tag("Order items")
        .in(jsonBody[OrderItem])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[OrderItem])

    val delete: Endpoint[JwtToken, Long, AppError, SimpleMessage, Any] =
      endpoint.delete
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("orders" / "items" / path[Long]("orderItemId"))
        .tag("Order items")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[SimpleMessage])

    val batchCreate: Endpoint[JwtToken, (OrderId, List[NewOrderItem]), AppError, List[OrderItem], Any] =
    endpoint.post
      .description("Batch creation of items in existing order")
      .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
      .mapSecurityIn(header => JwtToken(header))(_.value)
      .in("orders" / path[OrderId]("orderId)") / "items")
      .withTags(List("Order", "Order items"))
      .in(jsonBody[List[NewOrderItem]])
      .errorOut(jsonBody[AppError])
      .out(jsonBody[List[OrderItem]])
  }

  object basket {
    val findById: Endpoint[JwtToken, BasketId, AppError, Option[Basket], Any] =
      endpoint.get
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("baskets" / path[BasketId]("basketId"))
        .tag("Basket")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Option[Basket]])

    val create: Endpoint[JwtToken, NewBasket, AppError, Basket, Any] =
      endpoint.post
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("baskets")
        .tag("Basket")
        .in(jsonBody[NewBasket])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Basket])

    val update: Endpoint[JwtToken, BasketUpdate, AppError, Basket, Any] =
      endpoint.put
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("baskets")
        .tag("Basket")
        .in(jsonBody[BasketUpdate])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Basket])

    val delete: Endpoint[JwtToken, BasketId, AppError, SimpleMessage, Any] =
      endpoint.delete
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("baskets" / path[BasketId]("basketId"))
        .tag("Basket")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[SimpleMessage])

    val products: Endpoint[JwtToken, BasketId, AppError, List[ProductBasket], Any] =
      endpoint.get
        .description("Find items in the specified basket")
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("baskets" / path[BasketId]("basketId") / "items")
        .withTags(List("Basket", "Basket items"))
        .errorOut(jsonBody[AppError])
        .out(jsonBody[List[ProductBasket]])
  }

  object basketitem {
    val findById: Endpoint[JwtToken, Long, AppError, Option[ProductBasket], Any] =
      endpoint.get
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("baskets" / "item" / path[Long]("basketProductId"))
        .tag("Basket items")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[Option[ProductBasket]])

    val create: Endpoint[JwtToken, NewProductBasket, AppError, ProductBasket, Any] =
      endpoint.post
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("baskets" / "item")
        .tag("Basket items")
        .in(jsonBody[NewProductBasket])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[ProductBasket])

    val update: Endpoint[JwtToken, ProductBasket, AppError, ProductBasket, Any] =
      endpoint.put
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("baskets" / "item")
        .tag("Basket items")
        .in(jsonBody[ProductBasket])
        .errorOut(jsonBody[AppError])
        .out(jsonBody[ProductBasket])

    val delete: Endpoint[JwtToken, Long, AppError, SimpleMessage, Any] =
      endpoint.delete
        .securityIn(auth.bearer[String](WWWAuthenticateChallenge.bearer))
        .mapSecurityIn(header => JwtToken(header))(_.value)
        .in("baskets" / "item" / path[Long]("basketProductId"))
        .tag("Basket items")
        .errorOut(jsonBody[AppError])
        .out(jsonBody[SimpleMessage])
  }

}
