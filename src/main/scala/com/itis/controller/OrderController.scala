package com.itis.controller

import cats.effect.IO
import com.itis.controller.EndpointUtils.authenticate
import com.itis.service.{OrderItemService, OrderService, TokenService}
import sttp.tapir.server.ServerEndpoint

trait OrderController {

  def createOrder: ServerEndpoint[Any, IO]

  def findOrderById: ServerEndpoint[Any, IO]

  def updateOrder: ServerEndpoint[Any, IO]

  def deleteOrder: ServerEndpoint[Any, IO]

  def orderProducts: ServerEndpoint[Any, IO]

  def all: List[ServerEndpoint[Any, IO]]
}

object OrderController {
  private final class Impl(orderService: OrderService, orderItemService: OrderItemService)
                          (implicit tokenService: TokenService) extends OrderController {

    override def createOrder: ServerEndpoint[Any, IO] =
      authenticate(endpoints.order.create)
        .serverLogic(principal => newOrder =>
          orderService.create(newOrder))

    override def findOrderById: ServerEndpoint[Any, IO] =
      authenticate(endpoints.order.findById)
        .serverLogic(principal =>
        orderId => orderService.findById(orderId))

    override def updateOrder: ServerEndpoint[Any, IO] =
      authenticate(endpoints.order.update)
        .serverLogic(principal =>
          order => orderService.update(order))


    override def deleteOrder: ServerEndpoint[Any, IO] =
      authenticate(endpoints.order.delete)
        .serverLogic(principal =>
          orderId => orderService.delete(orderId))

    override def orderProducts: ServerEndpoint[Any, IO] =
      authenticate(endpoints.order.items)
        .serverLogic(principal =>
        orderId => orderItemService.findByOrderId(orderId))

    override def all: List[ServerEndpoint[Any, IO]] = List(
      createOrder,
      findOrderById,
      updateOrder,
      deleteOrder,
      orderProducts
    )

  }

  def make(orderService: OrderService, orderItemService: OrderItemService)(implicit tokenService: TokenService): OrderController
  = new Impl(orderService, orderItemService)
}




