package com.itis.controller

import cats.effect.IO
import com.itis.controller.EndpointUtils.authenticate
import com.itis.service.{ProductService, TokenService}
import sttp.tapir.server.ServerEndpoint

trait ProductController {

  def createProduct: ServerEndpoint[Any, IO]

  def findProductById: ServerEndpoint[Any, IO]

  def updateProduct: ServerEndpoint[Any, IO]

  def deleteProduct: ServerEndpoint[Any, IO]

  def findAll: ServerEndpoint[Any, IO]

  def filter: ServerEndpoint[Any, IO]

  def all: List[ServerEndpoint[Any, IO]]
}

object ProductController {
  private final class Impl(productService: ProductService)
                          (implicit tokenService: TokenService) extends ProductController {

    override def createProduct: ServerEndpoint[Any, IO] =
      authenticate(endpoints.product.create)
        .serverLogic(principal =>
          newProduct => productService.createProduct(newProduct))

    override def findProductById: ServerEndpoint[Any, IO] =
      authenticate(endpoints.product.findById)
        .serverLogic(principal =>
          productId => productService.findProductById(productId))

    override def updateProduct: ServerEndpoint[Any, IO] =
      authenticate((endpoints.product.update))
        .serverLogic(principal =>
          product => productService.updateProduct(product))

    override def deleteProduct: ServerEndpoint[Any, IO] =
      authenticate(endpoints.product.delete)
        .serverLogic(principal =>
          productId => productService.deleteProduct(productId))

    override def findAll: ServerEndpoint[Any, IO] =
      authenticate(endpoints.product.findAll)
        .serverLogic(principal =>
          unit => productService.findAll())

    override def filter: ServerEndpoint[Any, IO] =
      authenticate(endpoints.product.filter)
        .serverLogic(principal =>
          filter => productService.filterProducts(filter))

    override def all: List[ServerEndpoint[Any, IO]] = List(
      createProduct,
      findProductById,
      updateProduct,
      deleteProduct,
      findAll,
      filter
    )


  }

  def make(productService: ProductService)
          (implicit tokenService: TokenService): ProductController = new Impl(productService)
}




