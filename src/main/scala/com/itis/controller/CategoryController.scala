package com.itis.controller

import cats.effect.IO
import com.itis.controller.EndpointUtils.authenticate
import com.itis.service.{CategoryService, ProductService, TokenService}
import sttp.tapir.server.ServerEndpoint

trait CategoryController {

  def createCategory: ServerEndpoint[Any, IO]

  def findCategoryById: ServerEndpoint[Any, IO]

  def updateCategory: ServerEndpoint[Any, IO]

  def deleteCategory: ServerEndpoint[Any, IO]

  def findAll: ServerEndpoint[Any, IO]

  def findProductByCategoryId: ServerEndpoint[Any, IO]

  def all: List[ServerEndpoint[Any, IO]]
}

object CategoryController {
  private final class Impl(categoryService: CategoryService, productService: ProductService)
                          (implicit tokenService: TokenService) extends CategoryController {

    override def createCategory: ServerEndpoint[Any, IO] =
      authenticate(endpoints.category.create)
      .serverLogic { principal => category => categoryService.createCategory(category)}


    override def findCategoryById: ServerEndpoint[Any, IO] =
      authenticate(endpoints.category.findById)
        .serverLogic(principal => categoryId => categoryService.findByCategoryId(categoryId))

    override def updateCategory: ServerEndpoint[Any, IO] =
      authenticate(endpoints.category.update)
        .serverLogic(principal => category => categoryService.updateCategory(category))

    override def deleteCategory: ServerEndpoint[Any, IO] =
      authenticate(endpoints.category.delete)
        .serverLogic(principal => categoryId => categoryService.deleteCategory(categoryId))

    override def findAll: ServerEndpoint[Any, IO] =
      authenticate(endpoints.category.all)
        .serverLogic(principal => _ => categoryService.findAll())

    override def findProductByCategoryId: ServerEndpoint[Any, IO] =
      authenticate(endpoints.category.products)
        .serverLogic(principal => categoryId => productService.findByCategoryId(categoryId))

    override def all: List[ServerEndpoint[Any, IO]] = List(
      createCategory,
      findCategoryById,
      updateCategory,
      deleteCategory,
      findAll,
      findProductByCategoryId
    )

  }

  def make(categoryService: CategoryService, productService: ProductService)
          (implicit tokenService: TokenService): CategoryController = new Impl(categoryService, productService)
}




