CREATE TABLE IF NOT EXISTS files
(
    id            uuid      NOT NULL default gen_random_uuid(),
    primary key (id),
    original_name varchar   not null,
    storage_name  varchar   not null,
    size          bigint    not null,
    creation_time timestamp not null
);

CREATE TABLE IF NOT EXISTS users
(
    id              uuid         NOT NULL default gen_random_uuid(),
    PRIMARY KEY (id),
    name            varchar(255) NOT NULL,
    surname         varchar(255) NOT NULL,
    patronymic      varchar(255) NOT NULL,
    password_hash   varchar(255) NOT NULL,
    gender          varchar(255) NOT NULL,
    birth_date      timestamp    NOT NULL,
    email           varchar(255) NOT NULL unique,
    user_role       varchar(255) NOT NULL,
    phone           varchar(255) NOT NULL unique,
    profile_picture uuid references files (id),
    points          int8         not null default 0,
    city            varchar(255)
);
CREATE TABLE IF NOT EXISTS categories
(
    id        serial primary key,
    parent_id bigint references categories (id),
    title     varchar(255) not null
);
CREATE TABLE IF NOT EXISTS products
(
    id            serial primary key,
    title         varchar(255) not null,
    seller_id     uuid         not null references users (id),
    price         int8         not null,
    category_id   bigint       not null references categories (id),
    quantity      int          not null,
    description   varchar(255),
    creation_time timestamp    not null
);
CREATE TABLE IF NOT EXISTS product_files
(
    product_id bigint not null references products (id),
    file_id    uuid   not null references files (id),
    unique (product_id, file_id)
);
CREATE TABLE IF NOT EXISTS addresses
(
    id           serial primary key,
    full_address varchar(255) not null
);
CREATE TABLE IF NOT EXISTS orders
(
    id            serial primary key,
    address_id    bigint    not null references addresses (id),
    customer_id   uuid      not null references users (id),
    total_price   int8      not null,
    creation_time timestamp not null
);
CREATE TABLE IF NOT EXISTS statuses
(
    id   serial primary key,
    name varchar(255) unique not null
);
CREATE TABLE IF NOT EXISTS order_items
(
    id            serial primary key,
    product_id    bigint not null references products (id),
    order_id      bigint not null references orders (id),
    status_id     bigint not null references statuses (id),
    quantity      int8   not null,
    delivery_time timestamp
);
CREATE TABLE IF NOT EXISTS baskets
(
    id          serial primary key,
    customer_id uuid      not null references users (id),
    total_price int8      not null,
    update_time timestamp not null
);
CREATE TABLE IF NOT EXISTS product_basket
(
    id         serial primary key,
    product_id bigint not null references products (id),
    basket_id  bigint not null references baskets (id),
    quantity   int8   not null
);
alter table orders drop column if exists total_price;
alter table baskets drop column if exists total_price;
alter table users alter column surname drop not null;
alter table users alter column patronymic drop not null;
alter table users alter column gender drop not null;
alter table users alter column birth_date drop not null;
